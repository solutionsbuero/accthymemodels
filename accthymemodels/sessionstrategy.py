import uuid
import datetime
import secrets
import hashlib
from pydantic import ValidationError
from typing import Union, Optional, Tuple
from accthymemodels.mediator import DataMediator
from accthymemodels.interfaces import SessionInterface
from accthymemodels.models.dbbase import DatabaseInit
from accthymemodels.models.accthymeconfig import SessionConfig
from accthymemodels.models.dbmodels import DBEmployee, ResetToken, Session
from accthymemodels.models.requests import ResetRequest, TokenizedResetRequest, LoginRequest


class SessionStrategy(SessionInterface):

	def __init__(self, mediator: DataMediator):
		self.mediator = mediator
		self.config = self.mediator.get_config()
		self.dbinit = DatabaseInit()
		self.dbinit.initalize_db(self.config)
		self.dbinit.create_tables([DBEmployee, ResetToken, Session])
		if self.config.session is None:
			self.config.session = SessionConfig()

	# TODO: Expand for user levels, return (uuid, enum);
	#  or mediate to complete Employee Object => would need access to dispatcher from mediator
	def check_cookie(self, cookie: str) -> uuid.UUID:
		for i in Session.select().where(Session.valid_until < datetime.datetime.now()):
			i.delete_instance()
		q = Session.select().where(Session.token == cookie)
		if len(q) == 0:
			raise PermissionError("Unauthorized")
		q[0].valid_until = datetime.datetime.now() + datetime.timedelta(
			seconds=self.config.session.session_lifetime)
		q[0].save()
		return q[0].employee.unique

	def create_session(self, req: LoginRequest) -> (str, int):
		unique = SessionStrategy._check_credentials(req.email, req.password)
		if unique is None:
			raise PermissionError("Invalid credentials")
		new_cookie = secrets.token_urlsafe(32)
		lifetime = self.config.session.session_lifetime
		valid_until = datetime.datetime.now() + datetime.timedelta(seconds=lifetime)
		Session.create(employee=unique, token=new_cookie, valid_until=valid_until)
		return new_cookie, lifetime

	def destroy_session(self, cookie: str):
		q = Session.select().where(Session.token == cookie)
		if len(q) > 0:
			q[0].delete_instance()

	def create_reset_token(self, email: str) -> Optional[Tuple[str, str]]:  # UPDATE GENERICS
		e = DBEmployee.select(DBEmployee.unique, DBEmployee.salt, DBEmployee.pwhash).where(DBEmployee.email == email)
		if len(e) < 1:
			return None
		unique = e[0].unique
		new_token = secrets.token_urlsafe(16)
		valid_until = datetime.datetime.now() + datetime.timedelta(seconds=self.config.session.reset_lifetime)
		q = ResetToken.create(employee = unique, token=new_token, valid_until=valid_until)
		q.save()
		return email, new_token

	def password_reset(self, req: Union[ResetRequest, TokenizedResetRequest]):
		if isinstance(req, ResetRequest):
			unique = SessionStrategy._check_credentials(req.email, req.old_password)
			if unique is None:
				raise PermissionError("Invalid credentials")
		elif isinstance(req, TokenizedResetRequest):
			unique = SessionStrategy._check_reset_token(req.token, req.email)
			if unique is None:
				raise PermissionError("Invalid credentials")
		else:
			raise ValidationError("unacceptable data model")
		SessionStrategy._set_password(unique, req.new_password)

	@staticmethod
	def _check_credentials(email: str, password: str) -> Optional[uuid.UUID]:
		e = DBEmployee.select(DBEmployee.unique, DBEmployee.salt, DBEmployee.pwhash).where(DBEmployee.email == email)
		if len(e) < 1:
			return None
		e = e[0]
		hashed = hashlib.sha256((password + e.salt).encode()).hexdigest()
		if e.pwhash == hashed:
			return e.unique
		return None

	@staticmethod
	def _check_reset_token(token: str, email: str) -> Optional[uuid.UUID]:
		for i in ResetToken.select().where(ResetToken.valid_until < datetime.datetime.now()):
			i.delete_instance()
		t = ResetToken.select().where(ResetToken.token == token)
		if len(t) == 0:
			return None
		token_employee_unique = t[0].employee.unique
		employee = DBEmployee.select().where(DBEmployee.email == email)
		if len(employee) < 1:
			return None
		employee_unique = employee[0].unique
		if token_employee_unique == employee_unique:
			t = ResetToken.select().where(ResetToken.token == token)
			if len(t) == 0:
				t[0].delete_instance()
			return employee_unique
		return None

	@staticmethod
	def _set_password(unique: uuid.UUID, password: str):
		e = DBEmployee.get(DBEmployee.unique == unique)
		e.salt = secrets.token_urlsafe(8)
		salted = password + e.salt
		e.pwhash = hashlib.sha256(salted.encode()).hexdigest()
		e.save()
