class NoDataSourceException(Exception):
	pass


class AccDisabledException(Exception):
	pass


class DbDisabledException(Exception):
	pass


class DataConsistencyError(Exception):
	pass


class FileStructureError(Exception):
	pass
