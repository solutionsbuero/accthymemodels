import uuid
import datetime
import calendar
from playhouse.shortcuts import fn, model_to_dict
from typing import Optional, Union, List
from accthymemodels.exceptions import DataConsistencyError
from accthymemodels.mediator import DataMediator
from accthymemodels.interfaces import CPEInterface, TimeInterface
from accthymemodels.models.nested import NestedProject
from accthymemodels.models.dbbase import DatabaseInit
from accthymemodels.models.customer import Customer, CustomerCreation, CustomerList, NestedCustomer
from accthymemodels.models.project import Project, ProjectCreation
from accthymemodels.models.employees import Employee, EmployeeCreation, EmployeeList
from accthymemodels.models.dbmodels import DBCustomer, DBProject, DBEmployee, DBTimeEntry
from accthymemodels.models.timeentries import TimeEntry, TimeEntryList, TimeEntryCreation, TimeEntryMonth


class DBStrategy(CPEInterface, TimeInterface):

	def __init__(self, mediator: DataMediator):
		self.mediator = mediator
		self.config = self.mediator.get_config()
		self.dbinit = DatabaseInit()
		self.dbinit.initalize_db(self.config)
		self.dbinit.create_tables([DBCustomer, DBProject, DBEmployee, DBTimeEntry])

	'''
	Getters
	'''

	def get_customer_list(self) -> CustomerList:
		q = DBCustomer.select().order_by(fn.Lower(DBCustomer.name).asc())
		clist = CustomerList.get_empty()
		for i in q:
			data = Customer.from_orm(i)
			data.projects.sort(key=lambda x: x.name.lower())
			clist.append(data)
		return clist

	def get_customer_by_unique(self, unique: uuid.UUID) -> Optional[Customer]:
		q = DBCustomer.select().where(DBCustomer.unique == unique)
		if len(q) == 0:
			return None
		return Customer.from_orm(q[0])

	def get_project_by_unique(self, unique: uuid.UUID) -> Optional[Project]:
		q = DBProject.select().where(DBProject.unique == unique)
		if len(q) == 0:
			return None
		return Project.from_orm(q[0])

	def get_employee_list(self) -> EmployeeList:
		q = DBEmployee.select().order_by(fn.Lower(DBEmployee.name).asc())
		elist = EmployeeList.get_empty()
		for i in q:
			data = Employee.from_orm(i)
			elist.append(data)
		return elist

	def get_employee_by_unique(self, unique: uuid.UUID) -> Optional[Employee]:
		q = DBEmployee.select().where(DBEmployee.unique == unique)
		if len(q) == 0:
			return None
		return Employee.from_orm(q[0])

	def get_timeentry_list(
		self,
		employee: Union[uuid.UUID, Employee, None] = None,
		project: Union[uuid.UUID, Project, None] = None,
		customer: Union[uuid.UUID, Customer, None] = None,
		start: Optional[datetime.datetime] = None,
		end: Optional[datetime.datetime] = None,
		billable: Optional[bool] = None,
		invoiced: Optional[bool] = None,
		remunerated: Optional[bool] = None,
		orphans: Optional[bool] = None
	):
		employee = employee.unique if isinstance(employee, Employee) else employee
		customer = customer.unique if isinstance(customer, Customer) else customer
		project = project.unique if isinstance(project, Project) else project
		q = DBTimeEntry.select().order_by(DBTimeEntry.start)
		q = q if employee is None else q.where(DBTimeEntry.employee == employee)
		q = q if project is None else q.where(DBTimeEntry.project == project)
		q = q if start is None else q.where(DBTimeEntry.start >= start)
		q = q if end is None else q.where(DBTimeEntry.end <= end)
		q = q if billable is None else q.where(DBTimeEntry.billable == billable)
		q = q if invoiced is None else q.where(DBTimeEntry.invoiced == invoiced)
		q = q if remunerated is None else q.where(DBTimeEntry.remunerated == remunerated)
		entrylist = TimeEntryList.get_empty()
		for i in q:
			entrylist.append(TimeEntry.from_orm(i))
		entrylist.__root__ = entrylist.__root__ if customer is None else list(filter(lambda x: (x.project.customer == customer.unique), entrylist.__root__))
		entrylist.__root__ = entrylist.__root__ if orphans is None else list(filter(lambda x: (x.check_orphan() == orphans), entrylist.__root__))
		return entrylist

	def get_timeentry_by_id(self, entry_id: int) -> Optional[TimeEntry]:
		q = DBTimeEntry.select().where(DBTimeEntry.id == entry_id)
		if len(q) == 0:
			return None
		return TimeEntry.from_orm(q[0])

	'''
	Setters: Customer
	'''

	def save_customer(self, c: Union[Customer, CustomerCreation]) -> Customer:
		if c.unique is None:
			c.unique = uuid.uuid1()
		q = DBCustomer.select().where(DBCustomer.unique == c.unique)
		datadict = c.dict()
		if len(q) == 0:
			rec = DBCustomer.create(**datadict)
		else:
			rec = q[0]
			rec.set_data(**datadict)
		rec.save()
		return Customer.from_orm(rec)

	def delete_customer(self, customer: Union[Customer, NestedCustomer]):
		q = DBCustomer.select().where(DBCustomer.unique == customer.unique)
		if len(q) > 0:
			if len(q[0].projects) > 0:
				raise DataConsistencyError("Customer still has projects")
			q[0].delete_instance()

	'''
	Setters: Project
	'''

	def save_project(self, p: Union[Project, ProjectCreation]) -> Project:
		if p.unique is None:
			p.unique = uuid.uuid1()
		q = DBProject.select().where(DBProject.unique == p.unique)
		datadict = p.dict()
		if isinstance(p.customer, NestedCustomer):
			datadict["customer"] = p.customer.unique
		if len(q) == 0:
			rec = DBProject.create(**datadict)
		else:
			rec = q[0]
			rec.set_data(**datadict)
		rec.save()
		return Project.from_orm(rec)

	def delete_project(self, project: Union[Project, NestedProject]):
		q = DBProject.select().where(DBProject.unique == project.unique)
		if len(q) > 0:
			q[0].delete_instance()

	'''
	Setters: Employees
	'''

	def save_employee(self, e: Union[Employee, EmployeeCreation]) -> Employee:
		# print("")
		# print("*** passed to saver:", e.yaml())
		if e.unique is None:
			e.unique = uuid.uuid1()
		q = DBEmployee.select().where(DBEmployee.unique == e.unique)
		# print("*** Received from db:")
		# for i in q:
		# 	print(model_to_dict(i))
		datadict = e.dict()
		if len(q) == 0:
			rec = DBEmployee.create(**datadict)
			# print("*** CREATED: {}".format(model_to_dict(rec)))
		else:
			rec = q[0]
			rec.set_data(**datadict)
			# print("*** UPDATED: {}".format(model_to_dict(rec)))
		rec.save()
		return Employee.from_orm(rec)

	def delete_employee(self, e: Employee):
		q = DBEmployee.select().where(DBEmployee.unique == e.unique)
		if len(q) > 0:
			q[0].delete_instance()

	'''
	Setters: Time entries
	'''

	def save_timeentry(self, t: Union[TimeEntry, TimeEntryCreation]) -> TimeEntry:
		q = DBTimeEntry.select().where(DBTimeEntry.id == t.id)
		datadict = t.dict()
		if isinstance(t.employee, Employee):
			datadict["employee"] = t.employee.unique
		if isinstance(t.project, Project):
			datadict["project"] = t.project.unique
		if len(q) == 0:
			rec = DBTimeEntry.create(**datadict)
		else:
			rec = q[0]
			rec.set_data(**datadict)
		rec.save()
		return TimeEntry.from_orm(rec)

	def delete_timeentry(self, t: TimeEntry):
		q = DBTimeEntry.select().where(DBTimeEntry.id == t.id)
		if len(q) > 0:
			q[0].delete_instance()

	'''
	Dangerzone
	'''

	def update_from_acc(self, clist: CustomerList, elist: EmployeeList):
		c, p, e = self._prepare_update()
		with self.dbinit.db.atomic():
			c, p, e = self._write_updated_data(clist, elist, c, p, e)
			self._remove_obsolete_data(c, p, e)

	@staticmethod
	def _prepare_update() -> (dict, dict, dict):  # UPDATE GENERICS
		old_customers = old_projects = old_employees = {}
		p = DBCustomer.select(DBCustomer.unique, DBCustomer.name)
		q = DBProject.select(DBProject.unique, DBProject.name, DBProject.customer)
		r = DBEmployee.select(DBEmployee.unique, DBEmployee.name)
		for i in p:
			old_customers.update({i.unique: i.name})
			#print(model_to_dict(i, backrefs=True))
		for i in q:
			old_projects.update({i.unique: (i.name, i.customer.name)})
			#print(model_to_dict(i, backrefs=True))
		for i in r:
			old_employees.update({i.unique: i.name})
		return old_customers, old_projects, old_employees

	def _write_updated_data(
		self,
		clist: CustomerList,
		elist: EmployeeList,
		old_customers: dict,
		old_projects: dict,
		old_employees: dict
	) -> (dict, dict, dict):  # UPDATE GENERICS
		for i in clist.__root__:
			self.save_customer(i)
			old_customers.pop(i.unique, None)
			for j in i.projects:
				# print(j.yaml())
				self.save_project(j)
				old_projects.pop(j.unique, None)
		for i in elist.__root__:
			self.save_employee(i)
			old_employees.pop(i.unique, None)
		return old_customers, old_projects, old_employees

	@staticmethod
	def _remove_obsolete_data(obs_customers: dict, obs_projects: dict, obs_employees: dict):
		for k, v in obs_projects.items():
			for result in DBProject.select().where(DBProject.unique == k).limit(1):
				for i in result.timeentries:
					i.orphan_project = v[0]
					i.orphan_customer = v[1]
					i.project = None
					i.save()
				result.delete_instance()
		for k, v in obs_customers.items():
			for result in DBCustomer.select().where(DBCustomer.unique == k).limit(1):
				result.delete_instance()
		for k, v in obs_employees.items():
			for result in DBEmployee.select().where(DBEmployee.unique == k).limit(1):
				for i in result.timeentries:
					i.orphan_employee = v
					i.employee = None
					i.save()
				result.delete_instance()

	def export_timeentries(self):
		q = DBTimeEntry.select().order_by(DBTimeEntry.start)
		first_year = q[0].start.year
		last_year = q[len(q) - 1].start.year
		entrylist = []
		for i in range(first_year, last_year + 1):
			for j in range(12):
				month = j + 1
				last_day = calendar.monthrange(i, month)[1]
				entries = self.get_timeentry_list(
					start=datetime.datetime(i, month, 1, 0, 0, 0),
					end=datetime.datetime(i, month, last_day, 23, 59, 59),
				)
				if len(entries.__root__) > 0:
					entrylist.append(
						TimeEntryMonth(
							year=i,
							month=month,
							entries=entries
						)
					)
		self.mediator.timeentries_to_acc(entrylist)
