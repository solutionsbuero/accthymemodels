import uuid
from pathlib import Path
from typing import Union, Optional, Tuple
from accthymemodels.accstrategy import AccStrategy
from accthymemodels.dbstrategy import DBStrategy
from accthymemodels.models.requests import ResetRequest, TokenizedResetRequest, LoginRequest
from accthymemodels.sessionstrategy import SessionStrategy
from accthymemodels.interfaces import CPEInterface, AssetInterface, TimeInterface, SessionInterface, GitInterface
from accthymemodels.models.customer import Customer, CustomerCreation, CustomerList
from accthymemodels.models.accthymeconfig import AccThymeConfig
from accthymemodels.mediator import DataMediator
from accthymemodels.models.employees import Employee, EmployeeCreation, EmployeeList
from accthymemodels.models.expense import Expense, ExpenseCreation
from accthymemodels.models.invoice import Invoice, InvoiceCreation
from accthymemodels.models.nested import NestedProject, NestedCustomer
from accthymemodels.models.project import Project, ProjectCreation
from accthymemodels.models.quote import Quote, QuoteCreation
from accthymemodels.models.reminder import Reminder, ReminderCreation
from accthymemodels.models.timeentries import TimeEntry, TimeEntryCreation, TimeEntryList
from accthymemodels.models.organization import Organization
from accthymemodels.placeholders import NoAccObj, NoDbObj, NoSessObj, NoGitObj
from accthymemodels.exceptions import NoDataSourceException
from accthymemodels.models.enums import ExportFormat


class Dispatcher(CPEInterface, AssetInterface, TimeInterface, SessionInterface, GitInterface):

	def __init__(self, config: AccThymeConfig):
		self.mediator = DataMediator(config)
		dbdisabled = True
		accdisabled = True

		if config.database is not None:
			self.dbstrategy = DBStrategy(self.mediator)
			self.mediator.register_dbfactory(self.dbstrategy)
			self.cpeobj = self.dbstrategy
			self.timeobj = self.dbstrategy
			self.sessobj = SessionStrategy(self.mediator)
			dbdisabled = False

		else:
			self.timeobj = NoDbObj()
			self.sessobj = NoSessObj()

		if config.acc is not None:
			self.accstrategy = AccStrategy(mediator=self.mediator)
			self.mediator.register_accfactory(self.accstrategy)
			self.cpeobj = self.accstrategy
			self.assetobj = self.accstrategy
			self.gitobj = self.accstrategy.git
			accdisabled = False

		else:
			self.assetobj = NoAccObj()
			self.gitobj = NoGitObj()

		if accdisabled and dbdisabled:
			raise NoDataSourceException("No valid data source specified")

	'''
	Customers, Projects, Employees
	'''

	def get_customer_list(self, force_update=False) -> CustomerList:
		if isinstance(self.cpeobj, AccStrategy):
			return self.cpeobj.get_customer_list(force_update=force_update)
		return self.cpeobj.get_customer_list()

	def get_customer_by_unique(self, unique: uuid.UUID) -> Optional[Customer]:
		return self.cpeobj.get_customer_by_unique(unique)

	def get_project_by_unique(self, unique: uuid.UUID) -> Optional[Project]:
		return self.cpeobj.get_project_by_unique(unique)

	def get_employee_list(self, force_update=False) -> EmployeeList:
		if isinstance(self.cpeobj, AccStrategy):
			return self.cpeobj.get_employee_list(force_update=force_update)
		return self.cpeobj.get_employee_list()

	def get_employee_by_unique(self, unique: uuid.UUID) -> Optional[Employee]:
		return self.cpeobj.get_employee_by_unique(unique)

	def save_customer(self, c: Union[Customer, CustomerCreation]) -> Customer:
		return self.cpeobj.save_customer(c)

	def delete_customer(self, customer: Union[Customer, NestedCustomer]):
		return self.cpeobj.delete_customer(customer)

	def save_project(self, p: Union[Project, ProjectCreation]) -> Project:
		return self.cpeobj.save_project(p)

	def delete_project(self, project: Union[Project, NestedProject]):
		return self.cpeobj.delete_project(project)

	def save_employee(self, e: Union[Employee, EmployeeCreation]) -> Employee:
		return self.cpeobj.save_employee(e)

	def delete_employee(self, e: Employee):
		return self.cpeobj.delete_employee(e)

	'''
	Assets
	'''

	def acc_update(self):
		return self.assetobj.acc_update()

	def get_asset_by_unique(self, unique: uuid.UUID, force_update: bool = False) -> Union[Quote, Invoice, Expense, Reminder, None]:
		return self.assetobj.get_asset_by_unique(unique)

	def get_organization(self):
		return self.assetobj.get_organization()

	def save_asset(self, asset: Union[Quote, Invoice, Expense, Reminder, QuoteCreation, InvoiceCreation, ExpenseCreation, ReminderCreation]) -> Union[Quote, Invoice, Expense, Reminder]:
		return self.assetobj.save_asset(asset)

	def delete_asset(self, asset: Union[Quote, Invoice, Expense, Reminder]):
		return self.assetobj.delete_asset(asset)

	def save_organization(self, org: Organization):
		return self.assetobj.save_organization(org)

	def get_expense_categories(self, force_update: bool = False):
		return self.assetobj.get_expense_categories()

	'''
	Time Entries
	'''

	def get_timeentry_list(self, *args, **kwargs):
		return self.timeobj.get_timeentry_list(*args, **kwargs)

	def get_timeentry_by_id(self, entry_id: int) -> Optional[TimeEntry]:
		return self.timeobj.get_timeentry_by_id(entry_id)

	def save_timeentry(self, t: Union[TimeEntry, TimeEntryCreation]) -> TimeEntry:
		return self.timeobj.save_timeentry(t)

	def delete_timeentry(self, t: TimeEntry):
		return self.timeobj.delete_timeentry(t)

	'''
	Session
	'''

	def check_cookie(self, cookie: str) -> uuid.UUID:
		return self.sessobj.check_cookie(cookie)

	def create_session(self, req: LoginRequest) -> (str, int):
		return self.sessobj.create_session(req)

	def destroy_session(self, cookie: str):
		return self.sessobj.destroy_session(cookie)

	def create_reset_token(self, email: str) -> Optional[Tuple[str, str]]:
		return self.sessobj.create_reset_token(email)

	def password_reset(self, req: Union[ResetRequest, TokenizedResetRequest]):
		return self.sessobj.password_reset(req)

	'''
	Git
	'''
	def check_repo(self):
		return self.gitobj.check_repo()

	def git_update_local(self):
		self.gitobj.git_update_local()

	def git_update_remote(self):
		self.gitobj.git_update_remote()

	def git_full_update(self):
		self.gitobj.git_full_update()

	def git_clean(self):
		self.gitobj.git_clean()

	def git_is_dirty(self) -> bool:
		return self.gitobj.git_is_dirty()

	'''
	Direct cache access
	'''
	def get_cache(self):
		return self.accstrategy.get_cache()

	def export_report(self, year: int, p: Path, form: ExportFormat):
		self.accstrategy.export_report(year, p, form)

	def get_ordered_invoices(self, year: Optional[int] = None):
		return self.accstrategy.get_ordered_invoices(year=year)

	def get_ordered_expenses(self, year: Optional[int] = None):
		return self.accstrategy.get_ordered_expenses(year=year)
