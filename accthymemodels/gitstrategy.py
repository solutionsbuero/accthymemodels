import os
import git
import logging
from accthymemodels.models.accthymeconfig import AccThymeConfig
from accthymemodels.logconf import LogConf
from accthymemodels.interfaces import GitInterface


class GitHandler(GitInterface):

	AUTO_COMMIT_MSG = "acc auto commit"

	def __init__(self, config: AccThymeConfig):
		self.config = config
		log = LogConf(config)
		self.logger = log.get_logger("Git")
		os.environ['GIT_LFS_SKIP_SMUDGE'] = '1'

	def check_repo(self) -> bool:
		try:
			git.Repo(self.config.acc.basepath)
			return True
		except (git.exc.InvalidGitRepositoryError, git.exc.NoSuchPathError):
			return False

	def clone(self):
		git.Repo.clone_from(self.config.acc.repo, self.config.acc.basepath)

	def pull(self):
		repo = git.Repo(self.config.acc.basepath)
		orig = repo.remotes.origin
		orig.pull()

	def git_update_local(self):
		try:
			if self.check_repo():
				self.pull()
			else:
				self.clone()
		except git.exc.GitError as e:
			self.logger.error("Exception on Git pull/clone: {}".format(e))
			if self.config.acc.ignore_git_errors:
				pass
			else:
				raise e

	def git_update_remote(self):
		try:
			repo = git.Repo(self.config.acc.basepath)
			repo.git.add(all=True)
			repo.index.commit(GitHandler.AUTO_COMMIT_MSG)
			origin = repo.remote(name="origin")
			origin.push()
		except git.exc.GitError as e:
			self.logger.error("Exception on Git commit/push: {}".format(e))
			if self.config.acc.ignore_git_errors:
				pass
			else:
				raise e

	def git_full_update(self):
		self.git_update_local()
		self.git_update_remote()

	def git_clean(self):
		repo = git.Repo(self.config.acc.basepath)
		repo.git.clean("-fd")
		repo.git.reset("--hard")

	def git_is_dirty(self) -> bool:
		repo = git.Repo(self.config.acc.basepath)
		return repo.is_dirty()
