if __name__ == "__main__":
    # execute only if run as a script
    raise RuntimeError("Module is not intended to run on its own. Import accthymemodels.Dispatcher from your application")
