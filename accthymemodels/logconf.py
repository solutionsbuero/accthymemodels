import logging
from logging.handlers import RotatingFileHandler
from accthymemodels.meta import Singleton
from accthymemodels.models.accthymeconfig import AccThymeConfig


class LogConf(metaclass=Singleton):

	def __init__(self, config=AccThymeConfig):
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		self.rotater = RotatingFileHandler(
			str(config.logging.file),
			mode='a',
			maxBytes=5*1024*1024,
			backupCount=1
		)
		self.rotater.setFormatter(formatter)
		self.rotater.setLevel(config.logging.level)

	def get_basic_logger(self) -> logging.Handler:
		return self.rotater

	def get_logger(self, name: str):
		logger = logging.getLogger(name)
		logger.addHandler(self.rotater)
		return logger
