from dataclasses import dataclass


@dataclass(frozen=True)
class FilenameConstants:
	organization: str = "acc.yaml"
	employees: str = "employees.yaml"
	customer: str = "customer.yaml"
	project: str = "project.yaml"


@dataclass(frozen=True)
class DirectoryConstants:
	projects: str = "projects"
	timetracking: str = "timetracking"
