import uuid
import yaml
import json
import operator
from pathlib import Path
from typing import Optional, Union, Type, Dict, List, Set

from accreport.report import ReportFactory

from accthymemodels.exceptions import *
from accthymemodels.constants import DirectoryConstants, FilenameConstants
from accthymemodels.mediator import DataMediator
from accthymemodels.gitstrategy import GitHandler
from accthymemodels.statichelpers import StaticHelpers
from accthymemodels.interfaces import CPEInterface, AssetInterface
from accthymemodels.models.customer import Customer, NestedCustomer, CustomerList, CustomerCreation
from accthymemodels.models.project import Project, NestedProject, ProjectCreation
from accthymemodels.models.quote import Quote, QuoteCreation
from accthymemodels.models.invoice import Invoice, InvoiceCreation
from accthymemodels.models.reminder import Reminder, ReminderCreation
from accthymemodels.models.miscrecord import MiscRecord, MiscRecordCreation
from accthymemodels.models.expense import Expense, ExpenseCreation
from accthymemodels.models.employees import Employee, EmployeeList, EmployeeCreation
from accthymemodels.models.timeentries import TimeEntryMonth
from accthymemodels.models.organization import Organization
from accthymemodels.models.enums import ExportFormat


class AccCache:

	def __init__(self):
		self.customer_list: CustomerList = CustomerList.get_empty()
		self.customers: Dict[uuid.UUID, Customer] = {}  # UPDATE GENERICS
		self.projects: Dict[uuid.UUID, Project] = {}  # UPDATE GENERICS
		self.assets: Dict[uuid.UUID, Union[Quote, Reminder, Invoice, Expense]] = {}  # UPDATE GENERICS
		self.expense_categories: Set[str] = set()  # UPDATE GENERICS
		self.employee_list: EmployeeList = EmployeeList.get_empty()
		self.employees: Dict[uuid.UUID, Employee] = {}  # UPDATE GENERICS
		self.years: set[int] = set()

	def register_year(self, year: int):
		pass


class AccStrategy(CPEInterface, AssetInterface):

	def __init__(self, mediator: DataMediator):
		self.mediator = mediator
		self.config = self.mediator.get_config()
		self.git: GitHandler = GitHandler(self.config)
		self._cache = AccCache()
		self.cache_outdated: bool = True
		self.project_dir = self.config.acc.basepath / DirectoryConstants.projects
		self.employee_file: Path = self.config.acc.basepath / FilenameConstants.employees
		self.initialize_structure()

	'''
	Helper Methods
	'''

	def initialize_structure(self):
		if self.config.acc.repo is not None and self.config.acc.auto_pull:
			self.git.git_update_local()
		basepath = self.config.acc.basepath
		basepath.mkdir(mode=0o770, parents=True, exist_ok=True)
		orgfile = basepath / FilenameConstants.organization
		if not orgfile.is_file():
			init_org = {
				"organization": json.loads(Organization.get_empty().json()),
				"journalConfig": {}
			}
			with orgfile.open(mode='w') as f:
				f.write(yaml.safe_dump(init_org, allow_unicode=True, indent=4))
		if not self.employee_file.is_file():
			with self.employee_file.open(mode='w') as f:
				f.write(yaml.safe_dump([]))
		self.project_dir.mkdir(mode=0o770, parents=True, exist_ok=True)
		(basepath / DirectoryConstants.timetracking).mkdir(mode=0o770, parents=True, exist_ok=True)

	def acc_update(self):
		self.cache_outdated = True
		self.get_cache()
		self.update_db()
		if self.config.acc.repo is not None and self.config.acc.auto_commit:
			self.git.git_full_update()

	'''
	Build cache
	'''

	def get_cache(self, force_update: bool = False) -> AccCache:
		if force_update or self.cache_outdated:
			if self.config.acc.repo is not None and self.config.acc.auto_pull:
				self.git.git_update_local()
			self._cache = AccCache()
			self._update_employee_cache()
			self._update_project_cache(self._cache.employees)
			self.cache_outdated = False
		return self._cache

	def _update_employee_cache(self):
		with self.employee_file.open('r') as f:
			yamldata = yaml.safe_load(f.read())
		for i in yamldata:
			e = Employee.parse_obj(i)
			self._cache.employee_list.append(e)
			self._cache.employees[e.unique] = e

	def _update_project_cache(self, employees: Optional[dict] = None):
		for child in self.project_dir.iterdir():
			customer_file = child / FilenameConstants.customer
			if customer_file.is_file():
				customer = Customer.from_yaml_file(customer_file, cache_ref=self._cache, employees=employees)
				self._cache.customer_list.append(customer)
		self._cache.customer_list.sort()

	'''
	Getters
	'''

	def get_customer_list(self, force_update=False) -> CustomerList:
		cache = self.get_cache(force_update=force_update)
		return cache.customer_list

	def get_customer_by_unique(self, unique: uuid.UUID, force_update: bool = False) -> Optional[Customer]:
		cache = self.get_cache(force_update=force_update)
		return cache.customers.get(unique, None)

	def get_project_by_unique(self, unique: uuid.UUID, force_update: bool = False) -> Optional[Project]:
		cache = self.get_cache(force_update=force_update)
		return cache.projects.get(unique, None)

	def get_asset_by_unique(self, unique: uuid.UUID, force_update: bool = False) -> Union[Quote, Invoice, Expense, Reminder, None]:
		cache = self.get_cache(force_update=force_update)
		return cache.assets.get(unique, None)

	def get_expense_categories(self, force_update=False):
		cache = self.get_cache(force_update=force_update)
		categories = list(cache.expense_categories)
		if None in categories:
			categories.remove(None)
		categories.sort()
		return categories

	def get_employee_list(self, force_update=False) -> EmployeeList:
		cache = self.get_cache(force_update=force_update)
		return cache.employee_list

	def get_employee_by_unique(self, unique: uuid.UUID, force_update: bool = False) -> Optional[Employee]:
		cache = self.get_cache(force_update=force_update)
		return cache.employees.get(unique, None)

	def get_organization(self) -> Organization:
		return Organization.get_from_acc(self.config.acc.basepath)

	def _get_ordered_financial(self, model, datekey: str, year: Optional[int] = None) -> list:
		cache = self.get_cache()
		year_dict = dict()
		all_years = set()
		for k, v in cache.assets.items():
			if type(v) == model:
				entry_date = getattr(v, datekey, None)
				entry_year = 0 if entry_date is None else getattr(entry_date, "year")
				year_list = year_dict.get(entry_year, [])
				year_list.append(v)
				year_dict[entry_year] = year_list
				all_years.add(entry_year)
		for k, v in year_dict.items():
			if k == 0:
				v.sort(key=lambda x: x.unique)
				pass
			else:
				v.sort(key=lambda x: (getattr(x, datekey), x.unique))
				pass
			for n, i in enumerate(v, start=1):
				i.ordered_id = "{}{}-{}".format(i.prefix(), k, n)
		if year is not None:
			return year_dict.get(year, [])
		all_years = list(all_years)
		all_years.sort()
		entry_list = []
		for i in all_years:
			entry_list = entry_list + year_dict[i]
		return entry_list

	def get_ordered_invoices(self, year: Optional[int] = None) -> list:
		return self._get_ordered_financial(Invoice, "sendDate", year=year)

	def get_ordered_expenses(self, year: Optional[int] = None, internal: Optional[bool] = None) -> list:
		ls = self._get_ordered_financial(Expense, "dateOfAccrual", year=year)
		if internal is None:
			return ls
		ls = [i for i in ls if i.internal == internal]
		return ls

	def get_ordered_miscrecords(self, year: Optional[int] = None) -> list:
		return self._get_ordered_financial(MiscRecord, "dateOfAccrual", year=year)

	'''
	Setters: Customer
	'''

	def save_customer(self, c: Union[Customer, CustomerCreation]) -> Customer:
		if isinstance(c, CustomerCreation):
			c = self._create_customer(c)
		c.save_to_acc()
		self.acc_update()
		return c

	def delete_customer(self, customer: Union[Customer, NestedCustomer]):
		to_delete = self.get_customer_by_unique(customer.unique)
		to_delete.delete_from_acc()
		self.acc_update()

	def _create_customer(self, customer: CustomerCreation) -> Customer:
		if customer.unique is None:
			customer.unique = uuid.uuid1()
		cdata = self.get_customer_by_unique(customer.unique)
		if cdata is None:
			datadict = customer.dict()
			datadict["yamlfile"] = self._create_new_customer_path(datadict["name"])
		else:
			datadict = cdata.dict()
			datadict.update(customer.dict())
		return Customer.parse_obj(datadict)

	def _create_new_customer_path(self, pname: str) -> Path:
		basepath = self.config.acc.basepath / DirectoryConstants.projects
		dirname = StaticHelpers.name_to_dirname(pname)
		customerpath = basepath / dirname
		n = 1
		while customerpath.is_dir():
			n = n + 1
			customerpath = basepath / (dirname + "-" + str(n))
		return customerpath / FilenameConstants.customer

	'''
	Setters: Project
	'''

	def save_project(self, p: Union[Project, ProjectCreation]) -> Project:
		if isinstance(p, ProjectCreation):
			p = self._create_project(p)
		p.save_to_acc()
		self.acc_update()
		return p

	def delete_project(self, project: Union[Project, NestedProject]):
		to_delete = self.get_project_by_unique(project.unique)
		if to_delete is not None:
			to_delete.delete_from_acc()
		self.acc_update()

	def _create_project(self, project: ProjectCreation) -> Project:
		customer = self.get_customer_by_unique(project.customer)
		if project.unique is None:
			project.unique = uuid.uuid1()
		pdata = self.get_project_by_unique(project.unique)
		if pdata is None:
			datadict = project.dict()
			datadict["yamlfile"] = self._create_new_project_path(datadict["name"], customer)
		else:
			datadict = pdata.dict()
			datadict.update(project.dict())
		datadict["customer"] = NestedCustomer.parse_obj(customer)
		return Project.parse_obj(datadict)

	@staticmethod
	def _create_new_project_path(pname: str, c: Union[Customer, NestedCustomer]) -> Path:
		basepath = c.yamlfile.parent
		dirname = StaticHelpers.name_to_dirname(pname)
		projectpath = basepath / dirname
		n = 1
		while projectpath.is_dir():
			n = n + 1
			projectpath = basepath / (dirname + "-" + n)
		return projectpath / FilenameConstants.project

	'''
	Setters: Assets
	'''

	def save_asset(self, asset: Union[Quote, Invoice, Expense, Reminder, QuoteCreation, InvoiceCreation, ExpenseCreation, ReminderCreation]) -> Union[Quote, Invoice, Expense, Reminder]:
		if isinstance(asset, (QuoteCreation, InvoiceCreation, ExpenseCreation, ReminderCreation, MiscRecordCreation)):
			asset = self._create_asset(asset)
		typedef = StaticHelpers.asset_type_switch(asset=asset)
		asset_found = False
		p = self.get_project_by_unique(asset.project.unique)
		for n, i in enumerate(getattr(p, typedef[1])):
			if str(asset.unique) == str(i.unique):
				asset_found = True
				getattr(p, typedef[1])[n] = asset
		if not asset_found:
			getattr(p, typedef[1]).append(asset)
		self.save_project(p)
		self.acc_update()
		return asset

	def delete_asset(self, asset: Union[Quote, Invoice, Expense, Reminder]):
		typedef = StaticHelpers.asset_type_switch(asset=asset)
		p = self.get_project_by_unique(asset.project.unique)
		if p is None:
			raise AttributeError("No matching project found")
		for n, i in enumerate(getattr(p, typedef[1])):
			if str(asset.unique) == str(i.unique):
				getattr(p, typedef[1]).pop(n)
				self.save_project(p)
				self.acc_update()
				return
		raise AttributeError("No matching asset found")

	def _create_asset(self, asset: Union[QuoteCreation, InvoiceCreation, ExpenseCreation, ReminderCreation]) -> Union[Quote, Invoice, Expense, Reminder]:
		typedef = StaticHelpers.asset_type_switch(asset=asset)
		project = self.get_project_by_unique(asset.project)
		if project is None:
			raise AttributeError("no matching project found")
		asset_dict = asset.dict()
		if asset.unique is None:
			asset_dict["unique"] = uuid.uuid1()
		else:
			available_data = self.get_asset_by_unique(asset.unique)
			if available_data is not None:
				asset_dict_outdated = available_data.dict()
				asset_dict_outdated.update(asset_dict)
				asset_dict = asset_dict_outdated
		asset_dict["project"] = NestedProject.parse_obj(project)
		return typedef[0].parse_obj(asset_dict)

	def save_organization(self, org: Organization):
		org.save_to_acc(self.config.acc.basepath)

	'''
	Setters: Employees
	'''

	def save_employee(self, e: Union[Employee, EmployeeCreation]) -> Employee:
		if isinstance(e, EmployeeCreation):
			e = self._create_employee(e)
		cache = self.get_cache()
		cache.employees[e.unique] = e
		self._save_list(AccStrategy._listify(cache.employees))
		return e

	def delete_employee(self, e: Employee):
		e = self.get_employee_by_unique(e.unique)
		if e is None:
			raise ValueError("Employee not found")
		cache = self.get_cache()
		cache.employees.pop(e.unique)
		self._save_list(AccStrategy._listify(cache.employees))

	def _create_employee(self, employee: EmployeeCreation) -> Employee:
		if employee.unique is None:
			employee.unique = uuid.uuid1()
		edata = self.get_employee_by_unique(employee.unique)
		if edata is None:
			datadict = employee.dict()
		else:
			datadict = edata.dict()
			datadict.update(employee.dict())
		return Employee.parse_obj(datadict)

	@staticmethod
	def _listify(empl_dict: Dict[uuid.UUID, Employee]) -> EmployeeList:  # UPDATE GENERICS
		empl_list = EmployeeList.get_empty()
		for k, v in empl_dict.items():
			empl_list.append(v)
		empl_list.sort()
		return empl_list

	def _save_list(self, empl_list: EmployeeList):
		with self.employee_file.open('w') as f:
			f.write(empl_list.yaml())
		self.acc_update()

	'''
	Updates & Mediation
	'''

	def update_db(self, force_update = False):
		cache = self.get_cache(force_update=force_update)
		self.mediator.acc_to_db(cache.customer_list, cache.employee_list)

	def save_timeentries(self, entrylist: List[TimeEntryMonth]):  # UPDATE GENERICS
		for i in entrylist:
			i.save(self.config.acc.basepath / DirectoryConstants.timetracking)

	'''
	Export functionalites
	'''

	def export_report(self, year: int, p: Path, form: ExportFormat):
		if not p.is_dir():
			raise ValueError("Export must go to a directory")
		org: Organization = self.get_organization()
		invoices = self.get_ordered_invoices(year)
		expenses = self.get_ordered_expenses(year)
		miscrec = self.get_ordered_miscrecords(year)
		if form == ExportFormat.PDF:
			self._export_list_pdf(invoices, p, org)
			self._export_list_pdf(expenses, p, org)
			self._export_list_pdf(miscrec, p, org)
		elif form == ExportFormat.YAML:
			self._export_list_yaml(invoices, p / "invoices.yaml")
			self._export_list_yaml(expenses, p / "expenses.yaml")
			self._export_list_yaml(miscrec, p / "misc_records.yaml")

	@staticmethod
	def _export_list_yaml(data: list, p: Path):
		datastr = ""
		for i in data:
			datastr = "{}{}\n".format(datastr, i.yaml())
		with p.open("w") as f:
			f.write(datastr)

	@staticmethod
	def _export_list_pdf(data: list, p: Path, org: Organization):
		for i in data:
			fpath = p / (i.ordered_id + ".pdf")
			report = ReportFactory.from_record(i, org)
			report.generate(str(fpath))

