import re
from typing import Union
import hashlib
import base64
from pathlib import Path
import uuid
from accthymemodels.models.enums import SubprojectKeys
from accthymemodels.models.yamlbase import BaseModel
from accthymemodels.models.quote import Quote, QuoteCreation
from accthymemodels.models.invoice import Invoice, InvoiceCreation
from accthymemodels.models.reminder import Reminder, ReminderCreation
from accthymemodels.models.expense import Expense, ExpenseCreation
from accthymemodels.models.miscrecord import MiscRecord, MiscRecordCreation


class StaticHelpers:

	@staticmethod
	def name_to_dirname(name: str) -> str:
		splited = name.split(" ")
		parts = list()
		for i in splited:
			parts.append(re.sub('[^A-Za-z0-9]+', '', i).lower())
		return "-".join(parts)

	@staticmethod
	def asset_type_switch(asset: Union[Quote, QuoteCreation, Invoice, InvoiceCreation, Expense, ExpenseCreation, Reminder, ReminderCreation, SubprojectKeys]) -> (BaseModel, str):
		if isinstance(asset, (Quote, QuoteCreation)) or asset == SubprojectKeys.QUOTE:
			return Quote, "quotes"
		elif isinstance(asset, (Invoice, InvoiceCreation)) or asset == SubprojectKeys.INVOICE:
			return Invoice, "invoices"
		elif isinstance(asset, (Expense, ExpenseCreation)) or asset == SubprojectKeys.EXPENSE:
			return Expense, "expenses"
		elif isinstance(asset, (Reminder, ReminderCreation)) or asset == SubprojectKeys.REMINDER:
			return Reminder, "reminders"
		elif isinstance(asset, (MiscRecord, MiscRecordCreation)) or asset == SubprojectKeys.MISCRECORD:
			return MiscRecord, "miscrecords"
		else:
			raise ValueError("Unknown data model")

	@staticmethod
	def check_pwpolicy(passwd) -> bool:
		return len(passwd) >= 8

	@staticmethod
	def shortgen(v: uuid.UUID) -> str:
		data = v.bytes
		hashed = hashlib.blake2b(data, digest_size=6).digest()
		return base64.b64encode(hashed).decode("utf-8")
