import uuid
from typing import Optional, List
from accthymemodels.models.accthymeconfig import AccThymeConfig
from accthymemodels.models.employees import EmployeeList, Employee
from accthymemodels.models.customer import CustomerList
from accthymemodels.models.timeentries import TimeEntryMonth


class DataMediator:

	def __init__(self, config: AccThymeConfig):
		self.config = config
		self.accfactory = None
		self.dbfactory = None
		self.sessionfactory = None

	def register_accfactory(self, factory):
		self.accfactory = factory

	def register_dbfactory(self, factory):
		self.dbfactory = factory

	def register_sessionfactory(self, factory):
		self.sessionfactory = factory

	def get_config(self) -> AccThymeConfig:
		return self.config

	def acc_to_db(self, clist: CustomerList, elist: EmployeeList):
		if self.config.database is not None:
			self.dbfactory.update_from_acc(clist, elist)

	def timeentries_to_acc(self, entrylist: List[TimeEntryMonth]):  # UPDATE GENERICS
		if self.config.acc is not None:
			self.accfactory.save_timeentries(entrylist)
