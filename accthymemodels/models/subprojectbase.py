import decimal
import datetime
import uuid
from iso4217 import Currency
from typing import Optional, Union, Any
from pathlib import Path
from pydantic import validator
from accthymemodels.models.enums import SubprojectKeys
from accthymemodels.models.yamlbase import UniqueRecord, YamlUniqueRecord
from accthymemodels.models.types.money import Money


class SubprojectRecord(UniqueRecord):
    project: Any
    subprojectkey: SubprojectKeys
    path: Optional[Path]


class YamlSubprojectRecord(YamlUniqueRecord):
    path: Optional[str] = None


class FinancialRecord(SubprojectRecord):
    figure: Optional[Money] = None
    dateOfSettlement: Optional[datetime.date] = None
    settlementTransactionId: Optional[str] = None
    ordered_id: Optional[str] = None


class YamlFinancialRecord(YamlSubprojectRecord):
    figure: Money
    dateOfSettlement: Optional[datetime.date]
    settlementTransactionId: Optional[str]


class TimedRecord(SubprojectRecord):
    revoked: bool = False
    daysOfGrace: Optional[int] = 30
    sendDate: Optional[datetime.date] = None


class YamlTimedRecord(YamlSubprojectRecord):
    revoked: bool
    daysOfGrace: Optional[int] = 30
    sendDate: Optional[datetime.date]
    extendedBy: Union[uuid.UUID, str, None] = None


class ExtendableRecord(TimedRecord):
    extendedBy: "Union[NestedReminder, None, uuid.UUID]" = None


class YamlExtendableRecord(YamlTimedRecord):
    extendedBy: "Union[YamlReminder, NestedReminder, None, uuid.UUID]" = None

    @validator("extendedBy", always=True)
    def reduce_extension(cls, v, values, **kwargs):
        if getattr(v, "unique", None) is not None:
            return v.unique
        else:
            return v


from accthymemodels.models.reminder import Reminder, YamlReminder, NestedReminder
FinancialRecord.update_forward_refs()
SubprojectRecord.update_forward_refs()
YamlExtendableRecord.update_forward_refs()