from typing import Optional, Union
import decimal
from iso4217 import Currency
import uuid
import datetime
from pathlib import Path
from accthymemodels.models.enums import SubprojectKeys
from accthymemodels.models.yamlbase import YamlModel
from accthymemodels.models.subprojectbase import FinancialRecord, YamlFinancialRecord, ExtendableRecord, YamlExtendableRecord
from accthymemodels.models.types.money import Money


class InvoiceCreation(YamlModel):
    unique: Optional[uuid.UUID] = None
    project: uuid.UUID
    name: str
    path: Union[None, Path, str]
    figure: Union[Money, str]
    dateOfSettlement: Union[None, datetime.date] = None
    settlementTransactionId: Optional[str] = None
    revoked: bool = False
    daysOfGrace: Optional[int] = 30
    sendDate: Optional[datetime.date] = None
    extendedBy: "Optional[uuid.UUID]" = None


class Invoice(FinancialRecord, ExtendableRecord):
    subprojectkey = SubprojectKeys.INVOICE

    @classmethod
    def prefix(cls) -> str:
        return "I-"


class YamlInvoice(YamlFinancialRecord, YamlExtendableRecord):
    pass


from accthymemodels.models.reminder import Reminder, NestedReminder
Invoice.update_forward_refs()
YamlInvoice.update_forward_refs()
