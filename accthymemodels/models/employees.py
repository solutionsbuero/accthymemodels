import uuid
from typing import List, Optional
from peewee import *
from pydantic import BaseModel
from accthymemodels.constants import FilenameConstants
from accthymemodels.models.dbbase import DBBase
from accthymemodels.models.yamlbase import YamlModel, UniqueRecord, AddressRecord, RecordList, YamlUniqueRecord


class EmployeeCreation(YamlModel):
	unique: Optional[uuid.UUID] = None
	name: str
	street: str = ""
	postalCode: str = ""
	place: str = ""
	email: str


class YamlEmployee(YamlUniqueRecord, AddressRecord):
	partyType: int = 0


class Employee(UniqueRecord, AddressRecord):
	pass

	@classmethod
	def prefix(cls) -> str:
		return "Y-"


class EmployeeList(RecordList):
	__root__: List[Employee] = []  # UPDATE GENERICS

	def append(self, e: Employee):
		self.__root__.append(e)