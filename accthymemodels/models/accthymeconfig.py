import os
import yaml
from pydantic import HttpUrl
import logging
from enum import Enum
from typing import Optional, List, Union
from pathlib import Path
from accthymemodels.models.yamlbase import YamlModel, AddressRecord


class DatabaseType(Enum):
	MYSQL: str = "mysql"
	SQLITE: str = "sqlite"


class LogConfig(YamlModel):
	file: Path = "/tmp/accthyme.log"
	encoding = "utf-8"
	level: int = logging.ERROR


class MysqlConfig(YamlModel):
	type: DatabaseType = DatabaseType.MYSQL
	name: str
	host: str = "localhost"
	user: str
	password: str
	port: int = 3306


class SqliteConfig(YamlModel):
	type: DatabaseType = DatabaseType.SQLITE
	path: Path


class SessionConfig(YamlModel):
	session_lifetime: int = 10800
	reset_lifetime: int = 1800


class AccConfig(YamlModel):
	basepath: Path
	repo: Optional[str] = None
	auto_commit: bool = False
	auto_pull: bool = False
	ignore_git_errors: bool = False


class AccThymeConfig(YamlModel):
	name: Optional[str] = None
	acc: Optional[AccConfig] = None
	database: Union[MysqlConfig, SqliteConfig, None] = None
	session: Optional[SessionConfig] = None
	logging: LogConfig = LogConfig()
