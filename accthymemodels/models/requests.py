from pydantic import BaseModel, validator
from accthymemodels.statichelpers import StaticHelpers


class LoginRequest(BaseModel):
	email: str
	password: str


class ResetBase(BaseModel):
	email: str
	new_password: str
	new_password2: str

	@validator('new_password2', always=True)
	def password_validation(cls, v, values, **kwargs):
		if not StaticHelpers.check_pwpolicy(v):
			raise ValueError("Password policy violation")
		if not (v == values["new_password"]):
			raise ValueError("Password repeat missmatch")
		return v


class ResetRequest(ResetBase):
	old_password: str


class TokenizedResetRequest(ResetBase):
	token: str
