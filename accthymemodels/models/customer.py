import yaml
import os
import uuid
import shutil
from pathlib import Path
from typing import List, Optional
from accthymemodels.exceptions import DataConsistencyError
from accthymemodels.models.yamlbase import YamlModel, UniqueRecord, AddressRecord, RecordList, YamlUniqueRecord
from accthymemodels.models.project import Project
from accthymemodels.constants import FilenameConstants
from accthymemodels.models.nested import NestedCustomer
from pydantic import ValidationError


class CustomerCreation(YamlModel):
	unique: Optional[uuid.UUID] = None
	name: str
	street: str = ""
	place: str = ""
	postalCode: str = ""
	email: str = ""


class YamlCustomer(YamlUniqueRecord, AddressRecord):
	partyType: int = 1


class Customer(NestedCustomer):
	projects: List[Project] = []  # UPDATE GENERICS

	@classmethod
	def prefix(cls) -> str:
		return "C-"

	def save_to_acc(self):
		yamlentry = YamlCustomer.parse_obj(self)
		self.yamlfile.parent.mkdir(exist_ok=True, parents=True, mode=0o770)
		with self.yamlfile.open("w") as f:
			f.write(yamlentry.yaml())

	def delete_from_acc(self):
		if len(self.projects) > 0:
			raise DataConsistencyError("Customer still has projects.")
		shutil.rmtree(self.yamlfile.parent)

	@classmethod
	def from_yaml_file(cls, yamlfile: Path, cache_ref=None, employees=None):
		with yamlfile.open('r') as f:
			datadict = yaml.safe_load(f.read())
		try:
			datadict["yamlfile"] = yamlfile
			nested = NestedCustomer.parse_obj(datadict)
			datadict["projects"] = Customer._collect_projects(nested=nested, cache_ref=cache_ref, employees=employees)
			customer = cls.parse_obj(datadict)
			if cache_ref is not None:
				cache_ref.customers[customer.unique] = customer
			return customer
		except (TypeError, AttributeError, ValidationError) as e:
			raise DataConsistencyError("{} on reading project file \"{}\".\nOriginal message:\n{}".format(type(e).__name__, str(yamlfile), str(e)))

	@staticmethod
	def _collect_projects(nested: NestedCustomer, cache_ref=None, employees=None) -> List[Project]:  # UPDATE GENERICS
		project_list = list()
		for child in nested.yamlfile.parent.iterdir():
			projectfile = child / FilenameConstants.project
			if projectfile.is_file():
				project = Project.from_yaml_file(projectfile, customer=nested, cache_ref=cache_ref, employees=employees)
				project_list.append(project)
		project_list.sort(key=lambda x: x.name.lower())
		return project_list


class CustomerList(RecordList):
	__root__: List[Customer] = []  # UPDATE GENERICS

	def append(self, c: Customer):
		self.__root__.append(c)

