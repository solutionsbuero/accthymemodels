import json
import yaml
import datetime
import uuid
import hashlib
import base64
from typing import Optional
from pathlib import Path
from pydantic import BaseModel, validator, root_validator
from peewee import ModelSelect
from pydantic.utils import GetterDict
from typing import Any, List
from accthymemodels.models.types.money import Money
# from accthymemodels.statichelpers import StaticHelpers


class PeeweeGetterDict(GetterDict):
	def get(self, key: Any, default: Any = None):
		res = getattr(self._obj, key, default)
		if isinstance(res, ModelSelect):
			return list(res)
		return res


class YamlModel(BaseModel):

	@staticmethod
	def prepare_datetime(v):
		print("---")
		print(v)
		print("---")
		v.replace(microsecond=0).isoformat()
		return v

	@classmethod
	def from_yaml(cls, yamlstr):
		yamldata = yaml.safe_load(yamlstr)
		return cls.parse_obj(yamldata)

	def yaml(self) -> str:
		return yaml.safe_dump(json.loads(self.json()), allow_unicode=True, indent=4)

	class Config:
		json_encoders = {
			datetime.datetime: lambda v: YamlModel.prepare_datetime(v),
			Money: lambda v: v.__repr__()
		}
		json_decoders = {
			datetime: lambda v: datetime.datetime.fromisoformat(v),
		}
		orm_mode = True
		getter_dict = PeeweeGetterDict

	@root_validator(pre=True)
	def read_all_isodates(cls, values):
		# possible_dates = ["dateOfAccrual", "dateOfSettlement", "sendDate"]
		possible_dates = ["dateOfAccrual", "sendDate"]
		for i in possible_dates:
			candidate = values.get(i, None)
			if candidate is None:
				continue
			if type(candidate) == str:
				if candidate == "":
					values[i] = None
				else:
					values[i] = datetime.datetime.fromisoformat(candidate)
		return values


class YamlUniqueRecord(YamlModel):
	unique: uuid.UUID
	name: str


class UniqueRecord(YamlUniqueRecord):
	unique: uuid.UUID
	short: Optional[str] = None

	@classmethod
	def prefix(cls) -> str:
		return "0-"

	@validator("short", always=True)
	def shortgen(cls, v, values, **kwargs):
		data = values.get("unique", None)
		if data is None:
			return ""
		data = data.bytes
		hashed = hashlib.blake2b(data, digest_size=6).digest()
		hashstr = base64.b64encode(hashed).decode("utf-8")
		return "{}{}-{}".format(cls.prefix(), hashstr[:4], hashstr[4:])


class DirectoryRecord(UniqueRecord):
	yamlfile: Optional[Path] = None


class AddressRecord(YamlModel):
	street: str = ""
	postalCode: str = ""
	place: str = ""
	email: str = ""


class RecordList(YamlModel):
	__root__: List[Any] = []  # UPDATE GENERICS

	@classmethod
	def get_empty(cls):
		return cls(__root__=[])

	def append(self, rec: Any):
		self.__root__.append(rec)

	def sort(self):
		self.__root__.sort(key=lambda x: x.name.lower())
