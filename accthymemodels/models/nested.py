from accthymemodels.models.enums import ProjectAlignment
from accthymemodels.models.yamlbase import YamlModel, UniqueRecord, DirectoryRecord, AddressRecord, RecordList


class NestedCustomer(DirectoryRecord, AddressRecord):
	pass

	@classmethod
	def prefix(cls) -> str:
		return "C-"


class NestedProject(DirectoryRecord):
	customer: "NestedCustomer"
	alignment: ProjectAlignment = ProjectAlignment.REGULAR

	@classmethod
	def prefix(cls) -> str:
		return "P-"


NestedCustomer.update_forward_refs()
