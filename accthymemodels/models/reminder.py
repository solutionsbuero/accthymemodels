import uuid
import datetime
from pathlib import Path
from typing import Optional, Union
from accthymemodels.models.enums import SubprojectKeys
from accthymemodels.models.yamlbase import YamlModel
from accthymemodels.models.subprojectbase import ExtendableRecord, YamlExtendableRecord, TimedRecord


class ReminderCreation(YamlModel):
    unique: Optional[uuid.UUID] = None
    project: uuid.UUID
    name: str
    path: Union[None, Path, str] = None
    revoked: bool = False
    daysOfGrace: Optional[int] = 30
    sendDate: Optional[datetime.date] = None
    extendedBy: "Optional[uuid.UUID]" = None


class NestedReminder(TimedRecord):
    pass

    @classmethod
    def prefix(cls) -> str:
        return "R-"


class Reminder(ExtendableRecord):
    subprojectkey = SubprojectKeys.REMINDER

    @classmethod
    def prefix(cls) -> str:
        return "R-"


class YamlReminder(YamlExtendableRecord):
    pass


Reminder.update_forward_refs()
YamlReminder.update_forward_refs()
