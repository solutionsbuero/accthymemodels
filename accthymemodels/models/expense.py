import uuid
import datetime
import decimal
from pathlib import Path
from pydantic import validator
from typing import Optional, Union
from accthymemodels.models.employees import Employee
from accthymemodels.models.enums import SubprojectKeys, PaymentMode
from accthymemodels.models.yamlbase import YamlModel
from accthymemodels.models.subprojectbase import FinancialRecord, YamlFinancialRecord
from accthymemodels.models.types.money import Money


class ExpenseCreation(YamlModel):
	unique: Union[uuid.UUID, None] = None
	project: uuid.UUID
	name: str
	path: Union[None, Path, str]
	dateOfAccrual: Optional[datetime.date]
	billable: bool = False
	advancedBy: Optional[uuid.UUID] = None
	expenseCategory: Optional[str] = None
	internal: bool = False
	paymentMode: PaymentMode = PaymentMode.BANKTRANSFER
	figure: Union[Money, str]
	dateOfSettlement: Optional[datetime.date] = None
	settlementTransactionId: Optional[str] = None

	# @validator('dateOfSettlement', pre=True)
	# def time_validate(cls, v):
	# 	return datetime.datetime.fromisoformat(v)


class Expense(FinancialRecord):
	subprojectkey = SubprojectKeys.EXPENSE
	dateOfAccrual: Optional[datetime.date]
	billable: bool = False
	advancedBy: Optional[Union[Employee, uuid.UUID]] = None
	expenseCategory: Optional[str] = None
	internal: bool = False
	paymentMode: PaymentMode = PaymentMode.BANKTRANSFER

	@classmethod
	def prefix(cls) -> str:
		return "E-"


class YamlExpense(YamlFinancialRecord):
	dateOfAccrual: Optional[datetime.date]
	billable: bool = False
	advancedBy: Union[uuid.UUID, None] = None
	expenseCategory: Optional[str] = None
	internal: bool = False
	paymentMode: PaymentMode = PaymentMode.BANKTRANSFER

	@validator("advancedBy", always=True)
	def reduce_extension(cls, v, values, **kwargs):
		# print("Type: {}, Value: {}".format(type(v), v))
		if getattr(v, "unique", None) is not None:
			return v.unique
		else:
			return v
