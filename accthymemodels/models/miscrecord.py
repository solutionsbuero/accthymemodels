import uuid
import datetime
from pathlib import Path
from typing import Optional, Union
from accthymemodels.models.enums import SubprojectKeys, PaymentMode
from accthymemodels.models.yamlbase import YamlModel
from accthymemodels.models.subprojectbase import SubprojectRecord, YamlSubprojectRecord


class MiscRecordCreation(YamlModel):
	unique: Union[uuid.UUID, None] = None
	project: uuid.UUID
	name: str
	dateOfAccrual: Optional[datetime.date] = None
	ordered_id: Optional[str] = None
	path: Union[None, Path, str]


class MiscRecord(SubprojectRecord):
	subprojectkey = SubprojectKeys.MISCRECORD
	dateOfAccrual: Optional[datetime.date]
	ordered_id: Optional[str]

	@classmethod
	def prefix(cls) -> str:
		return "M-"


class YamlMiscRecord(YamlSubprojectRecord):
	dateOfAccrual: Optional[datetime.date]

