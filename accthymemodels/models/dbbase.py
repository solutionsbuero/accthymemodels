from peewee import Model, SqliteDatabase, MySQLDatabase, DatabaseProxy
from accthymemodels.models.accthymeconfig import AccThymeConfig, DatabaseType
from accthymemodels.meta import Singleton


class DatabaseInit(metaclass=Singleton):

	def __init__(self):
		self.proxy = DatabaseProxy()
		self.db = None

	def initalize_db(self, config: AccThymeConfig):
		if config.database.type == DatabaseType.SQLITE:
			self.db = SqliteDatabase(config.database.path)
		elif config.database.type == DatabaseType.MYSQL:
			self.db = MySQLDatabase(
				config.database.name,
				host=config.database.host,
				user=config.database.user,
				port=config.database.port,
				passwd=config.database.password
			)
		else:
			raise ValueError("Undefined Database")
		self.proxy.initialize(self.db)

	def create_tables(self, tables: list):
		self.db.create_tables(tables)


initializer = DatabaseInit()


class DBBase(Model):
	class Meta:
		database = initializer.proxy

	def set_data(self, **kwargs):
		for i in self._meta.fields.keys():
			if i in kwargs.keys():
				val = kwargs[i]
				setattr(self, i, val)
