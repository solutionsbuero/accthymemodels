import datetime
import uuid
import json
import calendar
from pathlib import Path
from pydantic import validator, ValidationError
from typing import Optional, List, Union
from accthymemodels.models.yamlbase import YamlModel
from accthymemodels.models.project import Project
from accthymemodels.models.customer import Customer
from accthymemodels.models.employees import Employee


class TimeEntryCreation(YamlModel):
	id: Optional[int]
	start: datetime.datetime
	end: Optional[datetime.datetime]
	project: Optional[uuid.UUID]
	employee: Optional[uuid.UUID]
	billable: bool = False
	invoiced: bool = False
	remunerated: bool = False
	desc: str = ""


class TimeEntry(YamlModel):
	id: int
	start: datetime.datetime
	end: Optional[datetime.datetime]
	project: Optional[Project]
	employee: Optional[Employee]
	orphan_customer: Optional[str] = None
	orphan_project: Optional[str] = None
	orphan_employee: Optional[str] = None
	billable: bool = False
	invoiced: bool = False
	remunerated: bool = False
	delta: Optional[datetime.timedelta]
	rounded_delta: Optional[int]
	desc: str = ""

	class Config:
		rounded_resolution = 900

	@validator('delta', always=True)
	def delta_exact(cls, v, values, **kwargs):
		if values["end"] is None:
			return None
		diff = values["end"].replace(microsecond=0) - values["start"].replace(microsecond=0)
		if diff.total_seconds() < 0:
			raise ValueError("Delta is negative")
		return diff

	@validator('rounded_delta', always=True)
	def delta_rounded(cls, v, values, **kwargs):
		if values["delta"] is None:
			return None
		diff = cls.delta_exact(v, values).total_seconds()
		to_add = cls.Config.rounded_resolution - (diff % cls.Config.rounded_resolution)
		return (diff + to_add) / 3600

	@validator('orphan_customer', always=True)
	def rm_orphan_customer(cls, v, values, **kwargs):
		if getattr(values['project'], "customer", None) is not None:
			return None
		return v

	@validator('orphan_employee', always=True)
	def rm_orphan_employee(cls, v, values, **kwargs):
		if values['employee'] is not None:
			return None
		return v

	@validator('orphan_project', always=True)
	def rm_orphan_state(cls, v, values, **kwargs):
		if values['project'] is not None:
			return None
		return v

	def check_orphan(self) -> bool:
		if None in [self.employee, self.project]:
			return True
		return False


class TimeEntryList(YamlModel):
	__root__: List[TimeEntry] = []

	@classmethod
	def get_empty(cls):
		return cls(__root__=[])

	def lined_json(self) -> str:
		iterstr = ""
		for n, i in enumerate(self.__root__):
			sep = "" if n == (len(self.__root__) - 1) else ","
			iterstr += "\t{}{}\n".format(i.json(), sep)

		return "[\n{}]".format(iterstr)

	@classmethod
	def from_lined_json(cls, jsonstr: str):
		return TimeEntryList(__root__=json.loads(jsonstr))

	def append(self, t: TimeEntry):
		self.__root__.append(t)

	def sort(self):
		self.__root__.sort(key=lambda x: x.start)

	def filter(
		self,
		employee: Union[uuid.UUID, Employee, None] = None,
		project: Union[uuid.UUID, Project, None] = None,
		customer: Union[uuid.UUID, Customer, None] = None,
		start: Optional[datetime.datetime] = None,
		end: Optional[datetime.datetime] = None,
		billable: Optional[bool] = None,
		invoiced: Optional[bool] = None,
		remunerated: Optional[bool] = None,
		orphans: Optional[bool] = None
	):
		employee = employee.unique if isinstance(employee, Employee) else employee
		customer = customer.unique if isinstance(customer, Customer) else customer
		project = project.unique if isinstance(project, Project) else project
		self.__root__ = self.__root__ if employee is None else list(filter(lambda x: (x.employee == employee.unique), self.__root__))
		self.__root__ = self.__root__ if customer is None else list(filter(lambda x: (x.project.customer == customer.unique), self.__root__))
		self.__root__ = self.__root__ if project is None else list(filter(lambda x: (x.project == project.unique), self.__root__))
		self.__root__ = self.__root__ if start is None else list(filter(lambda x: (x.start >= start), self.__root__))
		self.__root__ = self.__root__ if end is None else list(filter(lambda x: (x.end <= end), self.__root__))
		self.__root__ = self.__root__ if billable is None else list(filter(lambda x: (x.billable == billable), self.__root__))
		self.__root__ = self.__root__ if invoiced is None else list(filter(lambda x: (x.invoiced == invoiced), self.__root__))
		self.__root__ = self.__root__ if remunerated is None else list(filter(lambda x: (x.remunerated == remunerated), self.__root__))
		self.__root__ = self.__root__ if orphans is None else list(filter(lambda x: (x.check_orphan() == orphans), self.__root__))

	def sum_hours(self) -> float:
		hours = 0.0
		for i in self.__root__:
			if i.rounded_delta is not None:
				hours = hours + i.rounded_delta
		return hours

	def sum_seconds(self) -> int:
		sec = 0.0
		for i in self.__root__:
			if i.delta is not None:
				sec = sec + i.delta
		return round(sec)


class TimeEntryMonth(YamlModel):
	month: int
	year: int
	entries: TimeEntryList

	def save(self, basedir: Path):
		directory = basedir / str(self.year)
		filename = '{:02d}_{}.json'.format(self.month, calendar.month_name[self.month])
		directory.mkdir(exist_ok=True, parents=True)
		with (directory / filename).open('w') as f:
			f.write(self.entries.lined_json())
