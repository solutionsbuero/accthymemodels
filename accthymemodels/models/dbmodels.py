from peewee import *
from accthymemodels.models.dbbase import DBBase


class DBCustomer(DBBase):
	unique = UUIDField(primary_key=True)
	name = TextField()


class DBProject(DBBase):
	unique = UUIDField(primary_key=True)
	customer = ForeignKeyField(DBCustomer, backref="projects")  # No backref!
	name = TextField()


class DBEmployee(DBBase):
	unique = UUIDField(primary_key=True)
	name = TextField()
	email = TextField(unique=True)
	salt = TextField(default="")
	pwhash = TextField(default="")


class DBTimeEntry(DBBase):
	id = AutoField()
	start = TimestampField()
	end = TimestampField(null=True, default=None)
	project = ForeignKeyField(DBProject, backref="timeentries", null=True, default=None)
	employee = ForeignKeyField(DBEmployee, backref="timeentries", null=True, default=None)
	orphan_customer = TextField(null=True, default=None)
	orphan_project = TextField(null=True, default=None)
	orphan_employee = TextField(null=True, default=None)
	billable = BooleanField(default=False)
	invoiced = BooleanField(default=False)
	remunerated = BooleanField(default=False)
	desc = TextField(default="")


class SomeToken(DBBase):
	id = AutoField()
	employee = ForeignKeyField(DBEmployee)
	token = TextField(unique=True)
	valid_until = DateTimeField()


class ResetToken(SomeToken):
	pass


class Session(SomeToken):
	pass
