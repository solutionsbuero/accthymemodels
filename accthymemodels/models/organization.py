import os
import json
import yaml
from pydantic import HttpUrl
from enum import Enum
from typing import Optional, List, Union
from pathlib import Path
from iso4217 import Currency
from accthymemodels.models.yamlbase import YamlModel, AddressRecord
from accthymemodels.constants import FilenameConstants
from accthymemodels.exceptions import FileStructureError


class Organization(AddressRecord):
	logo: Union[Path, str, None] = None
	url: Optional[HttpUrl] = None
	name: str
	currency: Optional[Currency] = None

	@classmethod
	def get_empty(cls):
		return cls(name="")

	@classmethod
	def get_from_acc(cls, basepath: Path):
		with (basepath / FilenameConstants.organization).open("r") as f:
			yamldata = yaml.safe_load(f.read())
			orgdata = yamldata["organization"]
			if orgdata["logo"] is not None:
				orgdata["logo"] = basepath / orgdata["logo"]
		return cls.parse_obj(yamldata["organization"])

	def save_to_acc(self, basepath: Path):
		with (basepath / FilenameConstants.organization).open("r") as f:
			yamldata = yaml.safe_load(f.read())
		if self.logo is not None:
			if not str(basepath) in str(self.logo):   # UPDATE 3.9: Simplify with Path.is_relative_to()
				raise FileStructureError("Logo is not in base directory")
			self.logo = str(Path(self.logo).relative_to(basepath))
		yamldata['organization'] = json.loads(self.json())
		with (basepath / FilenameConstants.organization).open("w") as f:
			f.write(yaml.safe_dump(yamldata, allow_unicode=True, indent=4))
