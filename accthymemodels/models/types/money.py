import re
from iso4217 import Currency
from decimal import Decimal

from typing import Any


class Money(str):
    """
    Represents an amount of money as a ISO 4217 string. The algorithm tries to
    make balanced assumptions about the input. It is designed to allow as much
    input variation as possible without doing some implicit value changes.
    Therefore this class doesn't do any implicit rounding when loading a value
    from string.

    The input string should follow the
    [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217) standard. Following this
    the three letter currency code can placed before or after the amount
    optically spaced by any whitespace character available. Use a comma (`,`)
    or a dot (`.`) to split the minor units. The validation will fail if an
    input value have more minor units than allowed by the given currency. Minor
    units can be skipped if not needed (`EUR 23,4` is equivalent to
    `EUR 23,40`, `EUR 23,` and `23` will be parsed as `EUR 23,00). Zero amounts
    of money always have to be stated explicitly using a zero (ex.: `EUR 0`).

    Negative values are supported by prefixing the characteristic part of the
    amount with a minus sign `-`.
    """
    amount: int
    is_negative: bool
    currency: Currency

    def __new__(cls, value: str, **kwargs) -> object:
        return str.__new__(cls, value)

    def __init__(
        self,
        value: str,
        amount: int,
        is_negative: bool,
        currency: Currency
    ) -> None:
        str.__init__(value)
        self.amount = amount
        self.is_negative = is_negative
        self.currency = currency

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value: Any):
        if value.__class__ == cls:
            return value

        if not isinstance(value, str):
            raise ValueError("only can parse strings as money")

        exp = re.compile(
            r"(?P<code_1>[A-z]{3})?\s*"
            r"(?P<negative>-)?\s*(?P<characteristic>\d*)?([.,]"
            r"(?P<mantissa>\d*))?\s*"
            r"(?P<code_2>[A-z]{3})?"
        )
        match = exp.match(value)

        # Check if some amount is defined.
        if match.group("characteristic") == "" and \
                match.group("mantissa") is None:
            raise ValueError(
                f"input value '{value}' states no amount, to explicitly state "
                "a zero amount use 'XXX 0'"
            )

        if None not in match.group("code_1", "code_2"):
            raise ValueError(
                f"two currency code used ({match.group(1)} before and "
                f"{match.group(5)} after amount)"
            )

        code = match.group("code_1")
        if code is None:
            code = match.group("code_2")
        if code is None:
            raise ValueError(
                f"input value '{value}' doesn't define a currency code"
            )

        # This will throw an `ValueError` if invalid currency code is given.
        # Thus no custom error has to be raised.
        currency = Currency(code.upper())

        characteristic = 0
        if (rsl := match.group("characteristic")) is not None:
            characteristic = int(rsl)

        mantissa = "0"
        if (rsl := match.group("mantissa")) is not None and rsl != "":
            mantissa = rsl
        mantissa = mantissa.ljust(currency.exponent, "0")

        # Check if given minor part is compatible to the used currency. I.e.
        # it has at max as many digits as minor unit.
        if len(mantissa) > currency.exponent:
            raise ValueError(
                f"input value '{value}' contains more than the "
                f"{currency.exponent} digits in minor unit (e.g. cents) as "
                f"allowed in {currency.currency_name}."
            )

        amount = characteristic * (10 ** currency.exponent) + int(mantissa)
        is_negative = match.group("negative") is not None

        return cls(
            cls.__build_str(
                characteristic,
                mantissa,
                is_negative,
                currency,
            ),
            amount=amount,
            is_negative=is_negative,
            currency=currency
        )

    @staticmethod
    def __build_str(
        characteristic: int,
        mantissa: int,
        is_negative: bool,
        currency: Currency,
    ) -> str:
        minus_sign = ""
        if is_negative:
            minus_sign = "-"
        mantissa_fmt = str(mantissa).ljust(currency.exponent, "0")
        return f"{currency.value} {minus_sign}{characteristic}.{mantissa_fmt}"

    def __parts(self) -> (int, int):
        """
        Returns the characteristic (integer) and mantissa (fractional) part of
        a money amount.
        """
        exp = 10 ** self.currency.exponent
        mantissa = self.amount % exp
        characteristic = (self.amount - mantissa) // exp
        return (characteristic, mantissa)

    def __str__(self):
        parts = self.__parts()
        return self.__build_str(
            parts[0],
            parts[1],
            self.is_negative,
            self.currency,
        )

    def __repr__(self):
        return str(self)

    def __add__(self) -> "Money":
        raise NotImplementedError("addition is not implemented for Money")

    def __sub__(self) -> "Money":
        raise NotImplementedError("subtraction is not implemented for Money")

    def __mul__(self) -> "Money":
        raise NotImplementedError(
            "multiplication is not implemented for Money"
        )
