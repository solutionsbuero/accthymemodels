import datetime

import yaml
import shutil
import uuid
import itertools
from typing import Optional, List, Dict
from pathlib import Path
from accthymemodels.exceptions import FileStructureError, DataConsistencyError
from accthymemodels.models.yamlbase import YamlModel, UniqueRecord, YamlUniqueRecord
from accthymemodels.models.enums import SubprojectKeys, ProjectAlignment
from accthymemodels.models.invoice import Invoice, YamlInvoice
from accthymemodels.models.expense import Expense, YamlExpense
from accthymemodels.models.quote import Quote, YamlQuote
from accthymemodels.models.reminder import Reminder, YamlReminder, NestedReminder
from accthymemodels.models.miscrecord import MiscRecord, YamlMiscRecord
from accthymemodels.models.nested import NestedProject
from accthymemodels.statichelpers import StaticHelpers
from accthymemodels.models.employees import Employee
from pydantic import ValidationError


class ProjectCreation(YamlModel):
	unique: Optional[uuid.UUID] = None
	name: str
	customer: uuid.UUID
	alignment: ProjectAlignment = ProjectAlignment.REGULAR


class YamlProject(YamlUniqueRecord):
	quotes: List[YamlQuote] = []  # UPDATE GENERICS
	invoices: List[YamlInvoice] = []  # UPDATE GENERICS
	reminders: List[YamlReminder] = []  # UPDATE GENERICS
	expenses: List[YamlExpense] = []  # UPDATE GENERICS
	miscrecords: List[YamlMiscRecord] = []  # UPDATE GENERICS
	alignment: ProjectAlignment = ProjectAlignment.REGULAR


class Project(NestedProject):
	quotes: List[Quote] = []  # UPDATE GENERICS
	invoices: List[Invoice] = []  # UPDATE GENERICS
	reminders: List[Reminder] = []  # UPDATE GENERICS
	expenses: List[Expense] = []  # UPDATE GENERICS
	miscrecords: List[MiscRecord] = []  # UPDATE GENERICS

	@classmethod
	def prefix(cls) -> str:
		return "P-"

	@classmethod
	def from_yaml_file(cls, yamlfile: Path, customer, cache_ref=None, employees=None):
		with yamlfile.open('r') as f:
			datadict = yaml.safe_load(f.read())
		try:
			datadict["yamlfile"] = yamlfile
			datadict["customer"] = customer
			stripped_data = datadict.copy()
			for i in SubprojectKeys:
				stripped_data.pop(i.value, {})
			nested = NestedProject.parse_obj(stripped_data)
			stripped_data.update(Project._collect_assets(datadict, nested, cache_ref, employees))
			project = Project.parse_obj(stripped_data)
			if cache_ref is not None:
				cache_ref.projects[project.unique] = project
			return project
		except (TypeError, AttributeError, ValidationError) as e:
			raise DataConsistencyError("{} on reading project file \"{}\".\nOriginal message:\n{}".format(type(e).__name__, str(yamlfile), str(e)))

	@staticmethod
	def _collect_assets(datadict: dict, project: NestedProject, cache_ref=None, employees=None) -> Dict[str, list]:  # UPDATE GENERICS
		asset_dict = {}
		for i in SubprojectKeys:
			typedef = StaticHelpers.asset_type_switch(i)
			asset_dict[typedef[1]] = []
			for j in datadict.get(typedef[1], []):
				# Complete absolute path
				j["project"] = project
				if j["path"] is not None:
					j["path"] = project.yamlfile.parent / j["path"]
				# Link employees to expenses & cache expense categories
				if i == SubprojectKeys.EXPENSE:
					cache_ref.expense_categories.add(j["expenseCategory"])
					if employees is not None:
						Project._attach_employee(j, employees)
				asset = typedef[0].parse_obj(j)
				if i == SubprojectKeys.INVOICE:
					entry_date = getattr(asset, "sendDate", None)
					entry_year = entry_date.year if entry_date is not None else 0
					cache_ref.years.add(entry_year)
				if i == SubprojectKeys.EXPENSE:
					entry_date = getattr(asset, "dateOfAccrual", None)
					entry_year = entry_date.year if entry_date is not None else 0
					cache_ref.years.add(entry_year)
				# if cache_ref is not None:  # Reminders are not yet linked, can't cache just yet!
				# 	cache_ref.assets[asset.unique] = asset
				asset_dict[typedef[1]].append(asset)
		for i in SubprojectKeys:
			typedef = StaticHelpers.asset_type_switch(i)
			asset_dict[typedef[1]].sort(key=lambda x: x.name)
		Project._link_reminders(asset_dict)  # Recursion bug not fixed
		Project._cache_assets(asset_dict, cache_ref)  # Reminder-Linking done, now we can cache.
		return asset_dict

	@staticmethod
	def _attach_employee(expense: dict, employees: dict):
		if expense["advancedBy"] is None:
			return
		else:
			if isinstance(expense["advancedBy"], str):
				e = uuid.UUID(expense["advancedBy"])
			elif isinstance(expense["advancedBy"], Employee):
				tmp = expense["advancingParty"].unique
				e = employees.get(uuid.UUID(tmp), None)
			elif isinstance(expense["advancedBy"], dict):
				tmp = expense["advancedBy"]["unique"]
				e = employees.get(uuid.UUID(tmp), None)
			else:
				e = None
			expense["advancedBy"] = e

	@staticmethod
	def _link_reminders(asset_dict: dict):
		to_link = asset_dict["invoices"] + asset_dict["reminders"]
		for i in to_link:
			working_reminders = asset_dict["reminders"].copy()
			Project._rec_link_reminder(i, working_reminders)

	@staticmethod
	def _rec_link_reminder(asset, reminders):
		if asset.extendedBy is not None:
			for i in reminders:
				if asset.extendedBy == i.unique:
					asset.extendedBy = NestedReminder.parse_obj(i)
					# Project._rec_link_reminder(i, reminders)

	@staticmethod
	def _cache_assets(asset_dict: dict, cache_ref=None):
		if cache_ref is not None:
			for k, v in asset_dict.items():
				for i in v:
					cache_ref.assets[i.unique] = i

	@staticmethod
	def _sort_assets(asset_dict: dict):
		for i in SubprojectKeys:
			asset_dict[i.value].sort(key=lambda x: x.name.lower())

	def save_to_acc(self):
		datadict = self.dict()
		datadict = Project._yamlify_assets(datadict)
		yamlentry = YamlProject.parse_obj(datadict)
		self.yamlfile.parent.mkdir(parents=True, exist_ok=True)
		with self.yamlfile.open("w") as f:
			f.write(yamlentry.yaml())

	@staticmethod
	def _yamlify_assets(datadict: dict) -> dict:
		project_path = Path(datadict["yamlfile"]).parent
		for i in itertools.chain(datadict["quotes"], datadict["invoices"], datadict["expenses"], datadict["reminders"], datadict["miscrecords"]):
			if i["path"] is not None:
				if not str(project_path) in str(i["path"]):   # UPDATE 3.9: Simplify with Path.is_relative_to()
					raise FileStructureError("Asset is not in project directory")
				i["path"] = str(Path(i["path"]).relative_to(project_path))
		for i in itertools.chain(datadict["invoices"], datadict["reminders"]):
			if isinstance(i["extendedBy"], (YamlReminder, Reminder)):
				i["extendedBy"] = i["extendedBy"].unique
		return datadict

	def delete_from_acc(self):
		shutil.rmtree(self.yamlfile.parent)
