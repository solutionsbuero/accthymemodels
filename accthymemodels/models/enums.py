from enum import Enum


class ExportFormat(Enum):
    PDF = "pdf"
    YAML = "yaml"


class SubprojectKeys(Enum):
    INVOICE = "invoices"
    EXPENSE = "expenses"
    QUOTE = "quotes"
    REMINDER = "reminders"
    MISCRECORD = "miscrecords"


class PaymentMode(Enum):
    CREDITCARD = "creditcard"
    DEBITCARD = "debitcard"
    BANKTRANSFER = "banktransfer"
    CASH = "cash"


class PermissionLevel(Enum):
    DENIED = 0
    USER = 1
    ADMIN = 2


class ProjectAlignment(Enum):
    REGULAR = "Regular Project"
    AHV = "AHV"
    TAXES = "Taxes"
    PUBLIC_LIABLITIY = "Public Liability"
    CASUALTY_INSURANCE = "Casualty Insurance"
    INTERNAL_EXPENSES = "Internal Expenses"
    EMPLOYMENT = "Employment"

