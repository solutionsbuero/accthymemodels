from typing import Optional, Union
import uuid
import datetime
from pathlib import Path
from accthymemodels.models.enums import SubprojectKeys
from accthymemodels.models.yamlbase import YamlModel
from accthymemodels.models.subprojectbase import TimedRecord, YamlTimedRecord


class QuoteCreation(YamlModel):
	unique: Optional[uuid.UUID] = None
	project: uuid.UUID
	name: str
	path: Union[None, Path, str]
	sendDate: Optional[datetime.date] = None
	daysOfGrace: Optional[int] = 30
	revoked: bool = False
	accepted: bool = False
	revoked: bool = False
	sendDate: Optional[datetime.date] = None


class Quote(TimedRecord):
	subprojectkey = SubprojectKeys.QUOTE
	accepted: bool = False

	@classmethod
	def prefix(cls) -> str:
		return "Q-"

class YamlQuote(YamlTimedRecord):
	accepted: bool = False
