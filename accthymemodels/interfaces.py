from abc import ABC, abstractmethod
import uuid
from typing import Optional, Union, Tuple
import datetime
from pathlib import Path
from accthymemodels.models.customer import Customer, CustomerCreation, CustomerList
from accthymemodels.models.nested import NestedCustomer, NestedProject
from accthymemodels.models.project import Project, ProjectCreation
from accthymemodels.models.quote import Quote, QuoteCreation
from accthymemodels.models.invoice import Invoice, InvoiceCreation
from accthymemodels.models.reminder import Reminder, ReminderCreation
from accthymemodels.models.expense import Expense, ExpenseCreation
from accthymemodels.models.employees import Employee, EmployeeList, EmployeeCreation
from accthymemodels.models.timeentries import TimeEntry, TimeEntryList, TimeEntryCreation
from accthymemodels.models.requests import LoginRequest, ResetRequest, TokenizedResetRequest
from accthymemodels.models.organization import Organization


class CPEInterface(ABC):

	@abstractmethod
	def get_customer_list(self) -> CustomerList:
		pass

	@abstractmethod
	def get_customer_by_unique(self, unique: uuid.UUID) -> Optional[Customer]:
		pass

	@abstractmethod
	def get_project_by_unique(self, unique: uuid.UUID) -> Optional[Project]:
		pass

	@abstractmethod
	def get_employee_list(self) -> EmployeeList:
		pass

	@abstractmethod
	def get_employee_by_unique(self, unique: uuid.UUID) -> Optional[Employee]:
		pass

	@abstractmethod
	def save_customer(self, c: Union[Customer, CustomerCreation]) -> Customer:
		pass

	@abstractmethod
	def delete_customer(self, customer: Union[Customer, NestedCustomer]):
		pass

	@abstractmethod
	def save_project(self, p: Union[Project, ProjectCreation]) -> Project:
		pass

	@abstractmethod
	def delete_project(self, project: Union[Project, NestedProject]):
		pass

	@abstractmethod
	def save_employee(self, e: Union[Employee, EmployeeCreation]) -> Employee:
		pass

	@abstractmethod
	def delete_employee(self, e: Employee):
		pass


class AssetInterface(ABC):

	@abstractmethod
	def acc_update(self):
		pass

	@abstractmethod
	def get_asset_by_unique(self, unique: uuid.UUID, force_update: bool = False) -> Union[Quote, Invoice, Expense, Reminder, None]:
		pass

	@abstractmethod
	def get_organization(self):
		pass

	@abstractmethod
	def get_expense_categories(self, force_update: bool = False):
		pass

	@abstractmethod
	def save_asset(self, asset: Union[Quote, Invoice, Expense, Reminder, QuoteCreation, InvoiceCreation, ExpenseCreation, ReminderCreation]) -> Union[Quote, Invoice, Expense, Reminder]:
		pass

	@abstractmethod
	def delete_asset(self, asset: Union[Quote, Invoice, Expense, Reminder]):
		pass

	@abstractmethod
	def save_organization(self, org: Organization):
		pass

class TimeInterface(ABC):

	@abstractmethod
	def get_timeentry_list(
		self, *args, **kwargs) -> TimeEntryList:
		pass

	@abstractmethod
	def get_timeentry_by_id(self, entry_id: int) -> Optional[TimeEntry]:
		pass

	@abstractmethod
	def save_timeentry(self, t: Union[TimeEntry, TimeEntryCreation]) -> TimeEntry:
		pass

	@abstractmethod
	def delete_timeentry(self, t: TimeEntry):
		pass


class SessionInterface(ABC):

	@abstractmethod
	def check_cookie(self, cookie: str) -> uuid.UUID:
		pass

	@abstractmethod
	def create_session(self, req: LoginRequest) -> (str, int):
		pass

	@abstractmethod
	def destroy_session(self, cookie: str):
		pass

	@abstractmethod
	def create_reset_token(self, email: str) -> Optional[Tuple[str, str]]:
		pass

	@abstractmethod
	def password_reset(self, req: Union[ResetRequest, TokenizedResetRequest]):
		pass


class GitInterface(ABC):

	@abstractmethod
	def check_repo(self):
		pass

	@abstractmethod
	def git_update_local(self):
		pass

	@abstractmethod
	def git_update_remote(self):
		pass

	@abstractmethod
	def git_full_update(self):
		pass

	@abstractmethod
	def git_clean(self):
		pass

	@abstractmethod
	def git_is_dirty(self) -> bool:
		pass
