import uuid
from typing import Union, Optional, Tuple
from accthymemodels.interfaces import GitInterface
from accthymemodels.exceptions import AccDisabledException, DbDisabledException
from accthymemodels.interfaces import AssetInterface, TimeInterface, SessionInterface
from accthymemodels.models.expense import Expense, ExpenseCreation
from accthymemodels.models.invoice import Invoice, InvoiceCreation
from accthymemodels.models.organization import Organization
from accthymemodels.models.quote import Quote, QuoteCreation
from accthymemodels.models.reminder import Reminder, ReminderCreation
from accthymemodels.models.requests import LoginRequest
from accthymemodels.models.timeentries import TimeEntry, TimeEntryCreation, TimeEntryList


class NoAccObj(AssetInterface):

	def get_asset_by_unique(self, unique: uuid.UUID, force_update: bool = False) -> Union[Quote, Invoice, Expense, Reminder, None]:
		raise AccDisabledException("acc backend is disabled")

	def get_organization(self):
		raise AccDisabledException("acc backend is disabled")

	def get_expense_categories(self, force_update: bool = False):
		raise AccDisabledException("acc backend is diabled")

	def save_asset(self, asset: Union[Quote, Invoice, Expense, Reminder, QuoteCreation, InvoiceCreation, ExpenseCreation, ReminderCreation]) -> Union[Quote, Invoice, Expense, Reminder]:
		raise AccDisabledException("acc backend is disabled")

	def delete_asset(self, asset: Union[Quote, Invoice, Expense, Reminder]):
		raise AccDisabledException("acc backend is disabled")

	def save_organization(self, org: Organization):
		raise AccDisabledException("acc backend is disabled")

	def data_updated(self):
		raise AccDisabledException("acc backend is disabled")

	def acc_update(self):
		raise AccDisabledException("acc backend is disabled")


class NoDbObj(TimeInterface):

	def get_timeentry_list(self, *args, **kwargs) -> TimeEntryList:
		raise DbDisabledException("DB backend ist disabled")

	def get_timeentry_by_id(self, entry_id: int) -> Optional[TimeEntry]:
		raise DbDisabledException("DB backend ist disabled")

	def save_timeentry(self, t: Union[TimeEntry, TimeEntryCreation]) -> TimeEntry:
		raise DbDisabledException("DB backend ist disabled")

	def delete_timeentry(self, t: TimeEntry):
		raise DbDisabledException("DB backend ist disabled")


class NoSessObj(SessionInterface):

	def check_cookie(self, cookie: str) -> uuid.UUID:
		raise DbDisabledException("DB backend ist disabled")

	def create_session(self, req: LoginRequest) -> (str, int):
		raise DbDisabledException("DB backend ist disabled")

	@staticmethod
	def destroy_session(*args, **kwargs):
		raise DbDisabledException("DB backend ist disabled")

	def create_reset_token(self, email: str) -> Optional[Tuple[str, str]]:
		raise DbDisabledException("DB backend ist disabled")

	@staticmethod
	def password_reset(*args, **kwargs):
		raise DbDisabledException("DB backend ist disabled")


class NoGitObj(GitInterface):

	def check_repo(self):
		raise AccDisabledException("acc backend is disabled")

	def git_update_local(self):
		raise AccDisabledException("acc backend is disabled")

	def git_update_remote(self):
		raise AccDisabledException("acc backend is disabled")

	def git_full_update(self):
		raise AccDisabledException("acc backend is disabled")

	def git_clean(self):
		raise AccDisabledException("acc backend is disabled")

	def git_is_dirty(self) -> bool:
		raise AccDisabledException("acc backend is disabled")
