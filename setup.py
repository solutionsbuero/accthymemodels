from distutils.core import setup
from setuptools import find_packages

setup(
	name='accthymemodels',
	version='0.0.1',
	description='Data models for acc & thyme',
	author='Genossenschaft Solutionsbüro',
	author_email='info@buero.io',
	url='https://buero.io',
	packages=find_packages(),
	include_package_data=True,
	install_requires=[
		'pyyaml',
		'pydantic',
		'GitPython',
		'pytest',
		'iso4217',
		'accreport @ git+https://gitlab.com/solutionsbuero/accreport.git'
	]
)
