import unittest
import traceback
import shutil
import uuid
unittest.TestLoader.sortTestMethodsUsing = None
from tests.testdata import TestData
from accthymemodels.dbstrategy import DBStrategy
from accthymemodels.mediator import DataMediator


class TestDbTimeEntries(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		pass

	@classmethod
	def tearDownClass(cls) -> None:
		pass

	def setUp(self):
		self.config = TestData.config(use_acc=False, use_git=False, use_db=True)
		self.mediator = DataMediator(self.config)
		self.db = DBStrategy(self.mediator)
		self.mediator.register_dbfactory(self.db)

	def tearDown(self) -> None:
		self.config.database.path.unlink()
		pass

	def test_001_create_delete_entries(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.db.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.db.save_project(v)
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.db.save_employee(v)
		testdata = TestData.time_creation(projects=projects, employees=employees)
		timeentries = []
		for i in testdata:
			timeentries.append(self.db.save_timeentry(i))
		check = self.db.get_timeentry_list()
		self.assertEqual(4, len(check.__root__))
		check = self.db.get_timeentry_by_id(timeentries[0].id)
		self.assertEqual(timeentries[0].desc, check.desc)
		check = self.db.get_timeentry_by_id(timeentries[1].id)
		self.assertEqual(timeentries[1].billable, check.billable)
		check = self.db.get_timeentry_by_id(timeentries[2].id)
		self.assertEqual(testdata[2].employee, check.employee.unique)
		check = self.db.get_timeentry_by_id(timeentries[3].id)
		self.assertEqual(testdata[3].project, check.project.unique)
		self.db.delete_timeentry(timeentries[1])
		check = self.db.get_timeentry_list()
		self.assertEqual(3, len(check.__root__))
		check = self.db.get_timeentry_by_id(timeentries[0].id)
		self.assertEqual(timeentries[0].id, check.id)
		check = self.db.get_timeentry_by_id(timeentries[2].id)
		self.assertEqual(timeentries[2].id, check.id)
		check = self.db.get_timeentry_by_id(timeentries[1].id)
		self.assertIsNone(check)
		check = self.db.get_timeentry_by_id(10**7)
		self.assertIsNone(check)

	def test_002_modify_entries(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.db.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.db.save_project(v)
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.db.save_employee(v)
		testdata = TestData.time_creation(projects=projects, employees=employees)
		timeentries = []
		for i in testdata:
			timeentries.append(self.db.save_timeentry(i))
		testdata[0].id = timeentries[0].id
		testdata[0].desc = "Making a sandwich"
		self.db.save_timeentry(testdata[0])
		check = self.db.get_timeentry_by_id(testdata[0].id)
		self.assertEqual("Making a sandwich", check.desc)
		timeentries[1].desc = "Making an Everything Burrito"
		self.db.save_timeentry(timeentries[1])
		check = self.db.get_timeentry_by_id(timeentries[1].id)
		self.assertEqual("Making an Everything Burrito", check.desc)
