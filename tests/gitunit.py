import unittest
import traceback
import shutil
from pathlib import Path
from git.exc import GitError
unittest.TestLoader.sortTestMethodsUsing = None
from tests.testdata import TestData
from accthymemodels.accstrategy import AccStrategy
from accthymemodels.mediator import DataMediator
from accthymemodels.gitstrategy import GitHandler


class GitTests(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		pass

	@classmethod
	def tearDownClass(cls) -> None:
		pass

	def setUp(self):
		pass

	def tearDown(self) -> None:
		shutil.rmtree(self.config.acc.basepath, ignore_errors=True)
		traceback.print_exc()

	def test_001_pull_ok(self):
		self.config = TestData.config(use_acc=True, use_git=True, use_db=False)
		self.config.acc.repo = "git@gitlab.com:solutionsbuero/thymetest.git"
		self.config.acc.auto_pull = True
		self.config.acc.auto_commit = True
		self.config.acc.ignore_git_errors = True
		self.mediator = DataMediator(self.config)
		self.acc = AccStrategy(self.mediator)
		self.mediator.register_accfactory(self.acc)
		cl = self.acc.get_customer_list()
		self.assertEqual(3, len(cl.__root__))

	def test_002_pull_error(self):
		with self.assertRaises(GitError):
			self.config = TestData.config(use_acc=True, use_git=True, use_db=False)
			self.config.acc.repo = "ladida"
			self.config.acc.auto_pull = True
			self.config.acc.auto_commit = True
			self.config.acc.ignore_git_errors = False
			self.mediator = DataMediator(self.config)
			self.acc = AccStrategy(self.mediator)
			self.mediator.register_accfactory(self.acc)
			cl = self.acc.get_customer_list()

	def test_003_ignore_pull_error(self):
		self.config = TestData.config(use_acc=True, use_git=True, use_db=False)
		with Path(self.config.logging.file).open("w") as f:
			f.write("")
		self.config.acc.repo = "ladida"
		self.config.acc.auto_pull = True
		self.config.acc.auto_commit = True
		self.config.acc.ignore_git_errors = True
		self.mediator = DataMediator(self.config)
		self.acc = AccStrategy(self.mediator)
		self.mediator.register_accfactory(self.acc)
		cl = self.acc.get_customer_list()
		self.assertEqual(0, len(cl.__root__))
		with Path(self.config.logging.file).open("r") as f:
			data = f.read()
		self.assertGreater(len(data), 10)

	@unittest.skip("Too many pushes will make a mess of things")
	def test_004_push(self):
		self.config = TestData.config(use_acc=True, use_git=True, use_db=False)
		self.config.acc.repo = "git@gitlab.com:solutionsbuero/thymetest.git"
		self.config.acc.auto_pull = True
		self.config.acc.auto_commit = True
		self.config.acc.ignore_git_errors = False
		self.mediator = DataMediator(self.config)
		self.acc = AccStrategy(self.mediator)
		self.mediator.register_accfactory(self.acc)
		cl = self.acc.get_customer_list()
		self.assertEqual(2, len(cl.__root__))
		testdata = TestData.customer_creation()
		self.acc.save_customer(testdata["marcy"])
		cl = self.acc.get_customer_list()
		self.assertEqual(3, len(cl.__root__))
