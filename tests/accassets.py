import unittest
import traceback
import shutil
import uuid
unittest.TestLoader.sortTestMethodsUsing = None
from tests.testdata import TestData
from accthymemodels.exceptions import FileStructureError, DataConsistencyError
from accthymemodels.accstrategy import AccStrategy
from accthymemodels.mediator import DataMediator


class TestAccAssets(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		pass

	@classmethod
	def tearDownClass(cls) -> None:
		pass

	def setUp(self):
		self.config = TestData.config(use_acc=True, use_git=False, use_db=False)
		self.mediator = DataMediator(self.config)
		self.acc = AccStrategy(self.mediator)
		self.mediator.register_accfactory(self.acc)

	def tearDown(self) -> None:
		shutil.rmtree(self.config.acc.basepath)
		traceback.print_exc()

	def test_001_create_delete_assets(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.acc.save_project(v)
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.acc.save_employee(v)
		testdata = TestData.asset_creation(employees=employees, projects=projects)
		bad_asset = testdata.pop("bad")
		assets = {}
		for k, v in testdata.items():
			assets[k] = self.acc.save_asset(v)
		check = self.acc.get_customer_by_unique(customers["marcy"].unique)
		self.assertEqual(1, len(check.projects[1].quotes))
		self.assertEqual("Quote for writing a song", check.projects[1].quotes[0].name) ## TEST SUITE TEST
		self.assertEqual(2, len(check.projects[0].reminders))
		self.assertEqual("Pay me already!", check.projects[0].reminders[0].name)
		self.assertEqual("Please pay up", check.projects[0].reminders[1].name)
		self.assertEqual(2, len(check.projects[1].expenses))
		self.assertEqual("Buy base strings", check.projects[1].expenses[0].name)
		self.assertEqual("Buy bone pick", check.projects[1].expenses[1].name)
		self.assertEqual(1, len(check.projects[0].invoices))
		self.assertEqual("Button as eye ball", check.projects[0].invoices[0].name)
		self.acc.delete_asset(check.projects[1].quotes[0])
		self.acc.delete_asset(check.projects[0].reminders[0])
		self.acc.delete_asset(check.projects[1].expenses[0])
		self.acc.delete_asset(check.projects[0].invoices[0])
		check = self.acc.get_customer_by_unique(customers["marcy"].unique, force_update=True)
		self.assertEqual(0, len(check.projects[1].quotes))
		self.assertEqual(1, len(check.projects[0].reminders))
		self.assertEqual(1, len(check.projects[1].expenses))
		self.assertEqual(0, len(check.projects[0].invoices))
		self.assertEqual("Please pay up", check.projects[0].reminders[0].name)
		self.assertEqual("Buy bone pick", check.projects[1].expenses[0].name)

	def test_002_save_bad_asset(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.acc.save_project(v)
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.acc.save_employee(v)
		testdata = TestData.asset_creation(employees=employees, projects=projects)
		bad_asset = testdata.pop("bad")
		with self.assertRaises(FileStructureError):
			self.acc.save_asset(bad_asset)

	def test_003_modify_assets(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.acc.save_project(v)
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.acc.save_employee(v)
		testdata = TestData.asset_creation(employees=employees, projects=projects)
		bad_asset = testdata.pop("bad")
		assets = {}
		for k, v in testdata.items():
			assets[k] = self.acc.save_asset(v)
		testdata["song"].unique = assets["song"].unique
		testdata["song"].name = "Quote for recording a song"
		self.acc.save_asset(testdata["song"])
		check = self.acc.get_customer_by_unique(customers["marcy"].unique)
		self.assertEqual("Quote for recording a song", check.projects[1].quotes[0].name)
		check.projects[1].quotes[0].name = "Quote for recording two songs"
		self.acc.save_asset(check.projects[1].quotes[0])
		check = self.acc.get_customer_by_unique(customers["marcy"].unique)
		self.assertEqual("Quote for recording two songs", check.projects[1].quotes[0].name)
		testdata["eye"].unique = assets["eye"].unique
		testdata["eye"].name = "Found Hambo"
		self.acc.save_asset(testdata["eye"])
		check = self.acc.get_customer_by_unique(customers["marcy"].unique)
		self.assertEqual("Found Hambo", check.projects[0].invoices[0].name)
		check.projects[0].invoices[0].name = "Lost Hambo again"
		self.acc.save_asset(check.projects[0].invoices[0])
		check = self.acc.get_customer_by_unique(customers["marcy"].unique)
		self.assertEqual("Lost Hambo again", check.projects[0].invoices[0].name)
		testdata["remind"].unique = assets["remind"].unique
		testdata["remind"].name = "Where's my money?"
		self.acc.save_asset(testdata["remind"])
		assets["remind2"].name = "Where's my freaking money, you guys?"
		self.acc.save_asset(assets["remind2"])
		check = self.acc.get_customer_by_unique(customers["marcy"].unique)
		self.assertEqual("Where's my freaking money, you guys?", check.projects[0].reminders[0].name)
		self.assertEqual("Where's my money?", check.projects[0].reminders[1].name)
		testdata["pick"].unique = assets["pick"].unique
		testdata["pick"].name = "Buy steel pick"
		self.acc.save_asset(testdata["pick"])
		assets["string"].name = "Buy guitar strings"
		self.acc.save_asset(assets["string"])
		check = self.acc.get_customer_by_unique(customers["marcy"].unique)
		self.assertEqual("Buy guitar strings", check.projects[1].expenses[0].name)
		self.assertEqual("Buy steel pick", check.projects[1].expenses[1].name)

	@unittest.skip("Extension linking works slightly differently now because of recursion erros")
	def test_004_linking_assets(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.acc.save_project(v)
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.acc.save_employee(v)
		testdata = TestData.asset_creation(employees=employees, projects=projects)
		bad_asset = testdata.pop("bad")
		assets = {}
		for k, v in testdata.items():
			assets[k] = self.acc.save_asset(v)
		assets["remind"].extendedBy = assets["remind2"]
		re = self.acc.save_asset(assets["remind"])
		testdata["eye"].unique = assets["eye"].unique
		testdata["eye"].extendedBy = re.unique
		inv = self.acc.save_asset(testdata["eye"])
		bear = self.acc.get_project_by_unique(projects["bear"].unique)
		base = self.acc.get_project_by_unique(projects["base"].unique)
		self.assertEqual("Please pay up", bear.invoices[0].extendedBy.name)
		self.assertEqual("Pay me already!", bear.reminders[1].extendedBy.name)
		self.assertEqual("Pay me already!", bear.invoices[0].extendedBy.extendedBy.name)
		self.assertEqual("Finn the Human", base.expenses[0].advancedBy.name)
		self.assertIsNone(base.expenses[1].advancedBy)
