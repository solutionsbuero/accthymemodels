import json
import unittest

from pydantic import BaseModel

from accthymemodels.models.types.money import Money

unittest.TestLoader.sortTestMethodsUsing = None


class TestModel(BaseModel):
    cash: Money


class TestMoneyType(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls) -> None:
        pass

    def setUp(self):
        pass

    def tearDown(self) -> None:
        pass

    def test_proper_string(self):
        obj = TestModel(cash="CHF 2343.43")
        self.assertEqual(obj.cash.amount, 234343)
        self.assertEqual(obj.cash.currency.value, "CHF")
        self.assertEqual(obj.cash.currency.currency_name, "Swiss Franc")

    def test_decimal_separator(self):
        self.__check_against_str("CHF 2343.43", "CHF 2343.43")
        self.__check_against_str("CHF 2343,43", "CHF 2343.43")

    def test_code_location(self):
        self.__check_against_str("CHF 2343,43", "CHF 2343.43")
        self.__check_against_str("2343,43 CHF", "CHF 2343.43")

    def test_code_capitalization(self):
        self.__check_against_str("Chf 2343.43", "CHF 2343.43")
        self.__check_against_str("chF 2343.43", "CHF 2343.43")
        self.__check_against_str("chf 2343.43", "CHF 2343.43")
        self.__check_against_str("2343.43 Chf", "CHF 2343.43")
        self.__check_against_str("2343.43 chF", "CHF 2343.43")
        self.__check_against_str("2343.43 chf", "CHF 2343.43")

    def test_whitespaces(self):
        self.__check_against_str("CHF2343.43", "CHF 2343.43")
        self.__check_against_str("2343,43CHF", "CHF 2343.43")
        self.__check_against_str("CHF    2343.43", "CHF 2343.43")
        self.__check_against_str("2343,43  CHF", "CHF 2343.43")

    def test_mantissa(self):
        self.__check_against_str("CHF 2343.4", "CHF 2343.40")
        self.__check_against_str("CHF 2343.", "CHF 2343.00")
        self.__check_against_str("CLF 2343.05", "CLF 2343.0500")
        self.__check_against_str("CLF 2343.", "CLF 2343.0000")

        obj = TestModel(cash="CHF 2343.4")
        self.assertEqual(obj.cash.amount, 234340)

    def test_json(self):
        obj = TestModel(cash="CHF 2343.43")
        unmarshalled = json.loads(obj.json())
        self.assertEqual(unmarshalled["cash"], "CHF 2343.43")

    def test_yen(self):
        obj = TestModel(cash="jpy 456")
        self.assertEqual(obj.cash.amount, 456)

    def __check_against_str(self, raw: str, expected: str):
        obj = TestModel(cash=raw)
        self.assertEqual(obj.cash, expected)
