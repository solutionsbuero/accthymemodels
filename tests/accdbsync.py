import unittest
import traceback
import shutil
import uuid
from pathlib import Path
unittest.TestLoader.sortTestMethodsUsing = None
from tests.testdata import TestData
from accthymemodels.accstrategy import AccStrategy
from accthymemodels.dbstrategy import DBStrategy
from accthymemodels.mediator import DataMediator
from accthymemodels.models.dbmodels import DBProject, DBEmployee


class TestAccDbSync(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		pass

	@classmethod
	def tearDownClass(cls) -> None:
		pass

	def setUp(self):
		self.config = TestData.config(use_acc=True, use_git=False, use_db=True)
		self.mediator = DataMediator(self.config)
		self.acc = AccStrategy(self.mediator)
		self.db = DBStrategy(self.mediator)
		self.mediator.register_accfactory(self.acc)
		self.mediator.register_dbfactory(self.db)

	def tearDown(self) -> None:
		shutil.rmtree(self.config.acc.basepath)
		self.config.database.path.unlink()
		traceback.print_exc()

	def test_001_customers_project_sync(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.acc.save_project(v)
		# Check creation
		check = self.db.get_customer_list()
		self.assertEqual(4, len(check.__root__))
		self.assertEqual("Flame Princess", check.__root__[0].name)
		self.assertEqual("Marceline the Vampire Queen", check.__root__[2].name)
		self.assertEqual(2, len(check.__root__[2].projects))
		self.assertEqual("Find Hambo", check.__root__[2].projects[0].name)
		# print("\n**** Creation done ****\n")
		# Check removal
		self.acc.delete_project(projects["bear"])
		self.acc.delete_project(projects["lumps"])
		self.acc.delete_customer(customers["lumpy"])
		check = self.db.get_customer_list()
		self.assertEqual(3, len(check.__root__))
		self.assertEqual("Flame Princess", check.__root__[0].name)
		self.assertEqual("Marceline the Vampire Queen", check.__root__[1].name)
		self.assertEqual(1, len(check.__root__[1].projects))
		self.assertEqual("Play base guitar", check.__root__[1].projects[0].name)
		# print("\n**** Removal done ****\n")
		# Check modification
		customers["candy"].name = "Bonnibel"
		self.acc.save_customer(customers["candy"])
		projects["spy"].name = "Find Rattleballs"
		self.acc.save_project(projects["spy"])
		check = self.db.get_customer_list()
		self.assertEqual(3, len(check.__root__))
		self.assertEqual("Bonnibel", check.__root__[0].name)
		self.assertEqual("Flame Princess", check.__root__[1].name)
		self.assertEqual(2, len(check.__root__[0].projects))
		self.assertEqual("Find Rattleballs", check.__root__[0].projects[0].name)
		self.assertEqual("Fix Gumball Guardians", check.__root__[0].projects[1].name)
		# Check manual project move
		move_from = projects["burn"].yamlfile.parent
		move_to = customers["candy"].yamlfile.parent
		shutil.copytree(move_from, move_to / move_from.name)
		shutil.rmtree(move_from)
		self.acc.acc_update()
		check = self.db.get_customer_by_unique(customers["burny"].unique)
		self.assertEqual(0, len(check.projects))
		check = self.db.get_customer_by_unique(customers["candy"].unique)
		self.assertEqual(3, len(check.projects))
		self.assertEqual("Beat up Ice King", check.projects[0].name)

	def test_002_employee_sync(self):
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.acc.save_employee(v)
		# Check creation
		check = self.db.get_employee_list()
		self.assertEqual(3, len(check.__root__))
		self.assertEqual("BMO", check.__root__[0].name)
		self.assertEqual("Jake the Dog", check.__root__[2].name)
		# Check removal
		self.acc.delete_employee(employees["bmo"])
		check = self.db.get_employee_list()
		self.assertEqual(2, len(check.__root__))
		self.assertEqual("Finn the Human", check.__root__[0].name)
		self.assertEqual("Jake the Dog", check.__root__[1].name)
		# Check modification
		employees["finn"].name = "Davey"
		self.acc.save_employee(employees["finn"])
		check = self.db.get_employee_list()
		self.assertEqual(2, len(check.__root__))
		self.assertEqual("Davey", check.__root__[0].name)
		self.assertEqual("Jake the Dog", check.__root__[1].name)
		check = self.db.get_employee_by_unique(employees["finn"].unique)
		self.assertEqual("Davey", check.name)

	def test_003_backrefs(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.db.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.db.save_project(v)
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.db.save_employee(v)
		testdata = TestData.time_creation(projects=projects, employees=employees)
		timeentries = []
		for i in testdata:
			timeentries.append(self.db.save_timeentry(i))
		spy = DBProject.get(DBProject.unique == projects["spy"].unique)
		self.assertEqual(3, len(spy.timeentries))
		finn = DBEmployee.get(DBEmployee.unique == employees["finn"].unique)
		self.assertEqual(2, len(finn.timeentries))
		bear = DBProject.get(DBProject.unique == projects["bear"].unique)
		self.assertEqual(0, len(bear.timeentries))

	def test_004_orphan_data(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.acc.save_project(v)
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.acc.save_employee(v)
		testdata = TestData.time_creation(projects=projects, employees=employees)
		timeentries = []
		for i in testdata:
			timeentries.append(self.db.save_timeentry(i))
		test_entry = self.db.get_timeentry_by_id(timeentries[2].id)
		self.assertEqual("BMO", test_entry.employee.name)
		self.assertEqual("Check out my lumps!", test_entry.project.name)
		test_list = self.db.get_timeentry_list(orphans=True)
		self.assertEqual(0, len(test_list.__root__))
		self.acc.delete_employee(employees["bmo"])
		test_entry = self.db.get_timeentry_by_id(timeentries[2].id)
		self.assertEqual("BMO", test_entry.orphan_employee)
		test_list = self.db.get_timeentry_list(orphans=True)
		self.assertEqual(1, len(test_list.__root__))
		self.assertEqual("BMO", test_list.__root__[0].orphan_employee)
		timeentries[2].employee = employees["finn"].unique
		self.db.save_timeentry(timeentries[2])
		test_entry = self.db.get_timeentry_by_id(timeentries[2].id)
		self.assertIsNone(test_entry.orphan_employee)
		test_list = self.db.get_timeentry_list(orphans=True)
		self.assertEqual(0, len(test_list.__root__))
		self.acc.delete_project(projects["lumps"])
		test_entry = self.db.get_timeentry_by_id(timeentries[2].id)
		self.assertEqual("Check out my lumps!", test_entry.orphan_project)
		self.assertEqual("Lumpy Space Princess", test_entry.orphan_customer)
		test_list = self.db.get_timeentry_list(orphans=True)
		self.assertEqual(1, len(test_list.__root__))
		self.assertEqual("Lumpy Space Princess", test_list.__root__[0].orphan_customer)
		timeentries[2].project = projects["burn"].unique
		self.db.save_timeentry(timeentries[2])
		test_entry = self.db.get_timeentry_by_id(timeentries[2].id)
		self.assertIsNone(test_entry.orphan_project)
		self.assertIsNone(test_entry.orphan_customer)
		test_list = self.db.get_timeentry_list(orphans=True)
		self.assertEqual(0, len(test_list.__root__))
		self.acc.delete_employee(employees["finn"])
		shutil.rmtree(self.config.acc.basepath / "projects/flame-princess")
		self.acc.acc_update()
		test_entry = self.db.get_timeentry_by_id(timeentries[2].id)
		self.assertEqual("Beat up Ice King", test_entry.orphan_project)
		self.assertEqual("Flame Princess", test_entry.orphan_customer)
		self.assertEqual("Finn the Human", test_entry.orphan_employee)
		test_list = self.db.get_timeentry_list(orphans=True)
		self.assertEqual(3, len(test_list.__root__))
		self.assertEqual("Finn the Human", test_list.__root__[0].orphan_employee)
