import unittest
import traceback
import shutil
import uuid
unittest.TestLoader.sortTestMethodsUsing = None
from tests.testdata import TestData
from accthymemodels.accstrategy import AccStrategy
from accthymemodels.mediator import DataMediator
from accthymemodels.exceptions import DataConsistencyError


class TestAccCPE(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		pass

	@classmethod
	def tearDownClass(cls) -> None:
		pass

	def setUp(self):
		self.config = TestData.config(use_acc=True, use_git=False, use_db=False)
		self.mediator = DataMediator(self.config)
		self.acc = AccStrategy(self.mediator)
		self.mediator.register_accfactory(self.acc)

	def tearDown(self) -> None:
		shutil.rmtree(self.config.acc.basepath)
		traceback.print_exc()

	def test_001_create_delete_customers(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		check = self.acc.get_customer_list()
		self.assertEqual(4, len(check.__root__))
		self.assertEqual(customers.get("burny").name, check.__root__[0].name)
		self.assertEqual(customers["candy"].name, check.__root__[3].name)
		check = self.acc.get_customer_by_unique(customers["lumpy"].unique)
		self.assertEqual(customers["lumpy"].name, check.name)
		self.acc.delete_customer(customers["marcy"])
		check = self.acc.get_customer_list()
		self.assertEqual(3, len(check.__root__))
		self.assertEqual(customers.get("burny").name, check.__root__[0].name)
		self.assertEqual(customers["candy"].name, check.__root__[2].name)
		check = self.acc.get_customer_by_unique(customers["marcy"].unique)
		self.assertIsNone(check)
		check = self.acc.get_customer_by_unique(uuid.uuid4())
		self.assertIsNone(check)

	def test_002_modify_customers(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		check = self.acc.get_customer_list()
		marcy = check.__root__[2]
		testdata["marcy"].unique = marcy.unique
		testdata["marcy"].name = "Little Marcy"
		customers["candy"].name = "Bonnibel"
		self.acc.save_customer(testdata["marcy"])
		self.acc.save_customer(customers["candy"])
		check = self.acc.get_customer_by_unique(customers["marcy"].unique)
		self.assertEqual("Little Marcy", check.name)
		check = self.acc.get_customer_by_unique(customers["candy"].unique)
		self.assertEqual("Bonnibel", check.name)

	def test_003_create_delete_projects(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.acc.save_project(v)
		check = self.acc.get_customer_list()
		marcy = check.__root__[2]
		self.assertEqual(2, len(marcy.projects))
		self.assertEqual("Find Hambo", marcy.projects[0].name)
		self.assertEqual("Play base guitar", marcy.projects[1].name)
		check = self.acc.get_project_by_unique(projects["lumps"].unique)
		self.assertEqual("Check out my lumps!", check.name)
		self.assertEqual("Lumpy Space Princess", check.customer.name)
		self.acc.delete_project(projects["lumps"])
		check = self.acc.get_customer_list()
		self.assertEqual(0, len(check.__root__[1].projects))
		check = self.acc.get_project_by_unique(projects["lumps"].unique)
		self.assertIsNone(check)
		check = self.acc.get_project_by_unique(uuid.uuid4())
		self.assertIsNone(check)

	def test_004_delete_customer_exception(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		testdata = TestData.project_creation(customers)
		base = self.acc.save_project(testdata["base"])
		with self.assertRaises(DataConsistencyError):
			self.acc.delete_customer(customers["marcy"])
		self.acc.delete_project(base)
		self.acc.delete_customer(customers["marcy"])
		check = self.acc.get_customer_by_unique(customers["marcy"].unique)
		self.assertIsNone(check)

	def test_005_modify_projects(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.acc.save_project(v)
		guitar_unique = projects["base"].unique
		testdata["base"].unique = guitar_unique
		testdata["base"].name = "Sing while floating"
		self.acc.save_project(testdata["base"])
		check = self.acc.get_project_by_unique(guitar_unique)
		self.assertEqual("Sing while floating", check.name)
		projects["gum"].name = "Find Rattleballs"
		self.acc.save_project(projects["gum"])
		check = self.acc.get_project_by_unique(projects["gum"].unique)
		self.assertEqual("Find Rattleballs", check.name)
		move_from = projects["burn"].yamlfile.parent
		move_to = customers["candy"].yamlfile.parent
		shutil.copytree(move_from, move_to / move_from.name)
		shutil.rmtree(move_from)
		check = self.acc.get_customer_by_unique(customers["burny"].unique, force_update=True)
		self.assertEqual(0, len(check.projects))
		check = self.acc.get_customer_by_unique(customers["candy"].unique)
		self.assertEqual(3, len(check.projects))
		self.assertEqual("Beat up Ice King", check.projects[0].name)

	def test_006_create_delete_employees(self):
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.acc.save_employee(v)
		check = self.acc.get_employee_list()
		self.assertEqual(3, len(check.__root__))
		self.assertEqual("BMO", check.__root__[0].name)
		check = self.acc.get_employee_by_unique(employees["finn"].unique)
		self.assertEqual("Finn the Human", check.name)
		rm = self.acc.get_employee_by_unique(employees["bmo"].unique)
		self.acc.delete_employee(rm)
		check = self.acc.get_employee_list()
		self.assertEqual(2, len(check.__root__))
		self.assertEqual("Finn the Human", check.__root__[0].name)
		check = self.acc.get_employee_by_unique(rm.unique)
		self.assertIsNone(check)
		check = self.acc.get_employee_by_unique(uuid.uuid4())
		self.assertIsNone(check)

	def test_007_modify_employees(self):
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.acc.save_employee(v)
		testdata["jake"].unique = employees["jake"].unique
		testdata["jake"].name = "Farmland Jake"
		self.acc.save_employee(testdata["jake"])
		check = self.acc.get_employee_by_unique(employees["jake"].unique)
		self.assertEqual("Farmland Jake", check.name)
		employees["finn"].name = "Davey"
		self.acc.save_employee(employees["finn"])
		check = self.acc.get_employee_by_unique(employees["finn"].unique)
		self.assertEqual("Davey", check.name)
