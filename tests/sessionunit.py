import unittest
import traceback
import shutil
import uuid
import secrets
unittest.TestLoader.sortTestMethodsUsing = None
from pydantic import ValidationError
from tests.testdata import TestData
from accthymemodels.dbstrategy import DBStrategy
from accthymemodels.mediator import DataMediator
from accthymemodels.exceptions import DataConsistencyError
from accthymemodels.sessionstrategy import SessionStrategy
from accthymemodels.models.requests import ResetRequest, TokenizedResetRequest, LoginRequest


class TestSessionMethods(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		pass

	@classmethod
	def tearDownClass(cls) -> None:
		pass

	def setUp(self):
		self.config = TestData.config(use_acc=False, use_git=False, use_db=True)
		self.mediator = DataMediator(self.config)
		self.db = DBStrategy(self.mediator)
		self.session = SessionStrategy(self.mediator)
		self.mediator.register_dbfactory(self.db)

	def tearDown(self) -> None:
		self.config.database.path.unlink()
		pass

	def test_001_password_validation(self):
		with self.assertRaises(ValidationError):
			invalid_change_request = TokenizedResetRequest(
				email="finn@treehouse.ooo",
				new_password="PrincessBubblegum",
				new_password2="FlamePrincess",
				token="token"
			)
		with self.assertRaises(ValidationError):
			invalid_change_request = TokenizedResetRequest(
				email="finn@treehouse.ooo",
				new_password="abc",
				new_password2="abc",
				token="token"
			)

	def test_002_create_session(self):
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.db.save_employee(v)
		invalid_reset = self.session.create_reset_token("non@existing.com")
		self.assertIsNone(invalid_reset)
		reset_token = self.session.create_reset_token("finn@treehouse.ooo")
		self.assertTrue(isinstance(reset_token, tuple))
		self.assertTrue(isinstance(reset_token[0], str))
		self.assertTrue(isinstance(reset_token[1], str))
		denied_change_request = TokenizedResetRequest(
			email="finn@treehouse.ooo",
			new_password="PrincessBubblegum",
			new_password2="PrincessBubblegum",
			token="token"
		)
		with self.assertRaises(PermissionError):
			self.session.password_reset(denied_change_request)
		change_request = TokenizedResetRequest(
			email="finn@treehouse.ooo",
			new_password="PrincessBubblegum",
			new_password2="PrincessBubblegum",
			token=reset_token[1]
		)
		self.session.password_reset(change_request)
		denied_login = LoginRequest(
			email="finn@treehouse.ooo",
			password="FlamePrincess"
		)
		with self.assertRaises(PermissionError):
			self.session.create_session(denied_login)
		login = LoginRequest(
			email="finn@treehouse.ooo",
			password="PrincessBubblegum"
		)
		cookie = self.session.create_session(login)
		self.assertTrue(isinstance(cookie, tuple))
		self.assertTrue(isinstance(cookie[0], str))
		self.assertTrue(isinstance(cookie[1], int))
		cookie_unique = self.session.check_cookie(cookie[0])
		self.assertEqual(employees["finn"].unique, cookie_unique)
		self.session.destroy_session(cookie[0])
		with self.assertRaises(PermissionError):
			bad_cookie = self.session.check_cookie(cookie[0])
		denied_change_request = ResetRequest(
			email="finn@treehouse.ooo",
			new_password="HuntressWizard",
			new_password2="HuntressWizard",
			old_password="WRONG"
		)
		with self.assertRaises(PermissionError):
			self.session.password_reset(denied_change_request)
		change_request = ResetRequest(
			email="finn@treehouse.ooo",
			new_password="HuntressWizard",
			new_password2="HuntressWizard",
			old_password="PrincessBubblegum"
		)
		self.session.password_reset(change_request)
		with self.assertRaises(PermissionError):
			self.session.create_session(login)
		login = LoginRequest(
			email="finn@treehouse.ooo",
			password="HuntressWizard"
		)
		cookie = self.session.create_session(login)
		self.assertTrue(isinstance(cookie, tuple))
		self.assertTrue(isinstance(cookie[0], str))
		self.assertTrue(isinstance(cookie[1], int))
		cookie_unique = self.session.check_cookie(cookie[0])
		self.assertEqual(employees["finn"].unique, cookie_unique)
