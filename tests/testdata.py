import decimal
import datetime
import uuid
from pathlib import Path
from accthymemodels.models.customer import Customer, CustomerCreation, CustomerList
from accthymemodels.models.project import Project, ProjectCreation
from accthymemodels.models.employees import Employee, EmployeeCreation, EmployeeList
from accthymemodels.models.quote import Quote, QuoteCreation
from accthymemodels.models.invoice import InvoiceCreation, Invoice
from accthymemodels.models.expense import ExpenseCreation, Expense
from accthymemodels.models.reminder import ReminderCreation, Reminder
from accthymemodels.models.miscrecord import MiscRecord, MiscRecordCreation
from accthymemodels.models.timeentries import TimeEntryCreation
from accthymemodels.models.accthymeconfig import AccThymeConfig, SqliteConfig, AccConfig, SessionConfig
from accthymemodels.models.types.money import Money


class TestData:

	@staticmethod
	def employee_creation() -> dict:
		return {
			"bmo": EmployeeCreation(
				name="BMO",
				email="bmo@treehouse.ooo"
			),
			"finn": EmployeeCreation(
				name="Finn the Human",
				email="finn@treehouse.ooo"
			),
			"jake": EmployeeCreation(
				name="Jake the Dog",
				email="jake@treehouse.ooo"
			)
		}

	@staticmethod
	def customer_creation() -> dict:
		return {
			"marcy": CustomerCreation(
				name="Marceline the Vampire Queen"
			),
			"lumpy": CustomerCreation(
				name="Lumpy Space Princess"
			),
			"burny": CustomerCreation(
				name="Flame Princess"
			),
			"candy": CustomerCreation(
				name="Princess Bubblegum"
			)
		}

	@staticmethod
	def project_creation(customers: dict) -> dict:
		return {
			"base": ProjectCreation(
				name="Play base guitar",
				customer=customers.get("marcy").unique
			),
			"bear": ProjectCreation(
				name="Find Hambo",
				customer=customers.get("marcy").unique
			),
			"lumps": ProjectCreation(
				name="Check out my lumps!",
				customer=customers.get("lumpy").unique
			),
			"burn": ProjectCreation(
				name="Beat up Ice King",
				customer=customers.get("burny").unique
			),
			"spy": ProjectCreation(
				name="Spy on Candy People",
				customer=customers.get("candy").unique
			),
			"gum": ProjectCreation(
				name="Fix Gumball Guardians",
				customer=customers.get("candy").unique
			)
		}

	@staticmethod
	def asset_creation(projects: dict, employees: dict) -> dict:
		return {
			"song": QuoteCreation(
				name="Quote for writing a song",
				project=projects.get("base").unique
			),
			"bad": QuoteCreation(
				name="Wrong path",
				project=projects.get("base").unique,
				path=Path("/tmp/badfile.pdf")
			),
			"eye": InvoiceCreation(
				name="Button as eye ball",
				figure="chf1.50",
				project=projects.get("bear").unique,
				path=projects.get("bear").yamlfile.parent / "StakeInvoice.pdf"
			),
			"remind": ReminderCreation(
				name="Please pay up",
				project=projects.get("bear").unique
			),
			"remind2": ReminderCreation(
				name="Pay me already!",
				project=projects.get("bear").unique
			),
			"pick": ExpenseCreation(
				name="Buy bone pick",
				project=projects.get("base").unique,
				figure="15.00eur",
			),
			"string": ExpenseCreation(
				name="Buy base strings",
				project=projects.get("base").unique,
				figure="usd3.60",
				advancedBy=employees.get("finn").unique
				# advancedBy=None
			),
			"misc": MiscRecordCreation(
				name="new misc record",
				project=projects.get("base").unique
			)
		}

	@staticmethod
	def sorted_assets(projects: dict, employees: dict) -> list:
		return [
			ExpenseCreation(
				name="Expense 1 February",
				project=projects.get("base").unique,
				figure="usd3.60",
				dateOfAccrual="2021-02-10",
				advancedBy=None
			),
			ExpenseCreation(
				name="Expense 1.1 February same date",
				project=projects.get("base").unique,
				figure="usd3.60",
				dateOfAccrual="2021-02-10",
				advancedBy=None
			),
			ExpenseCreation(
				name="Expense 2 January",
				project=projects.get("base").unique,
				figure="usd3.60",
				dateOfAccrual="2021-01-10",
				advancedBy=None,
				internal=True
			),
			ExpenseCreation(
				name="Expense 3 March",
				project=projects.get("base").unique,
				figure="usd3.60",
				dateOfAccrual="2021-03-10",
				advancedBy=None,
				internal=True
			),
			ExpenseCreation(
				name="Expense Last Year",
				project=projects.get("base").unique,
				figure="usd3.60",
				dateOfAccrual="2020-02-10",
				advancedBy=None
			),
			ExpenseCreation(
				name="Expense No Date",
				project=projects.get("base").unique,
				figure="usd3.60",
				dateOfAccrual=None,
				advancedBy=None
			),
			ExpenseCreation(
				name="Expense No Date 2",
				project=projects.get("base").unique,
				figure="usd3.60",
				dateOfAccrual=None,
				advancedBy=None,
				internal=True
			),
			InvoiceCreation(
				name="Invoice 1 February",
				figure="chf1.50",
				project=projects.get("bear").unique,
				sendDate="2021-02-10",
				internal=True
			),
			InvoiceCreation(
				name="Invoice 2 January",
				figure="chf1.50",
				project=projects.get("bear").unique,
				sendDate="2021-01-10"
			),
			InvoiceCreation(
				name="Invoice 3 March",
				figure="chf1.50",
				project=projects.get("bear").unique,
				sendDate="2021-03-10",
				internal=True
			),
			InvoiceCreation(
				name="Invoice Last Year",
				figure="chf1.50",
				project=projects.get("bear").unique,
				sendDate="2020-02-10"
			),
			InvoiceCreation(
				name="Invoice No Date",
				figure="chf1.50",
				project=projects.get("bear").unique,
				sendDate=None
			)
		]

	@staticmethod
	def time_creation(projects: dict, employees: dict) -> list:
		an_our_ago = datetime.datetime.now() - datetime.timedelta(hours=1)
		return [
			TimeEntryCreation(
				start=datetime.datetime(1789, 7, 14, 18, 0, 0),
				end=datetime.datetime(1789, 7, 14, 18, 57, 0),
				desc="Finn spies on candy people: Default values",
				project=projects.get("spy").unique,
				employee=employees.get("finn").unique
			),
			TimeEntryCreation(
				start=datetime.datetime(2020, 1, 25, 9, 0, 0),
				end=datetime.datetime(2020, 1, 25, 9, 31, 0),
				desc="Jake spies on candy people: True bools",
				project=projects.get("spy").unique,
				employee=employees.get("jake").unique,
				billable=True,
				invoiced=True,
				remunerated=True
			),
			TimeEntryCreation(
				start=datetime.datetime(2020, 2, 25, 17, 0, 0),
				end=datetime.datetime(2020, 2, 25, 18, 00, 0),
				desc="BMO & LSP: Create orphans",
				project=projects.get("lumps").unique,
				employee=employees.get("bmo").unique
			),
			TimeEntryCreation(
				start=an_our_ago,
				desc="Finn: Open entry",
				project=projects.get("spy").unique,
				employee=employees.get("finn").unique
			)
		]

	@staticmethod
	def config(use_acc: bool, use_git: bool, use_db: bool):
		if use_acc:
			repourl = None if not use_git else "https://gitlab.com/solutionsbuero/thymetest.git"
			accconfig = AccConfig(
				basepath=Path("/tmp/accthymetest"),
				repo=repourl
			)
		else:
			accconfig = None
		dbconfig = None if not use_db else SqliteConfig(path=Path("/tmp/accthymetest.db"))
		session = None
		if use_db:
			session = SessionConfig(
				session_lifetime=30,
				reset_lifetime=30
			)
		return AccThymeConfig(acc=accconfig, database=dbconfig, session=session)
