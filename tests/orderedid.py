import unittest
import traceback
import shutil
import uuid
unittest.TestLoader.sortTestMethodsUsing = None
from tests.testdata import TestData
from accthymemodels.exceptions import FileStructureError, DataConsistencyError
from accthymemodels.accstrategy import AccStrategy
from accthymemodels.mediator import DataMediator


class TestOrderedAssets(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		pass

	@classmethod
	def tearDownClass(cls) -> None:
		pass

	def setUp(self):
		self.config = TestData.config(use_acc=True, use_git=False, use_db=False)
		self.mediator = DataMediator(self.config)
		self.acc = AccStrategy(self.mediator)
		self.mediator.register_accfactory(self.acc)

	def tearDown(self) -> None:
		shutil.rmtree(self.config.acc.basepath)
		traceback.print_exc()

	def test_ordered_expenses(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.acc.save_project(v)
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.acc.save_employee(v)
		assets = []
		testdata = TestData.sorted_assets(projects=projects, employees=employees)
		for i in testdata:
			assets.append(self.acc.save_asset(i))
		check = self.acc.get_ordered_expenses()
		self.assertEqual(len(check), 7)
		self.assertEqual(check[0].name, "Expense No Date")
		self.assertEqual(check[0].ordered_id, "E-0-1")
		self.assertEqual(check[1].name, "Expense No Date 2")
		self.assertEqual(check[1].ordered_id, "E-0-2")
		self.assertEqual(check[2].name, "Expense Last Year")
		self.assertEqual(check[2].ordered_id, "E-2020-1")
		check = self.acc.get_ordered_expenses(year=0)
		self.assertEqual(len(check), 2)
		self.assertEqual(check[0].name, "Expense No Date")
		self.assertEqual(check[0].ordered_id, "E-0-1")
		self.assertEqual(check[1].name, "Expense No Date 2")
		self.assertEqual(check[1].ordered_id, "E-0-2")
		check = self.acc.get_ordered_expenses(year=2020)
		self.assertEqual(len(check), 1)
		self.assertEqual(check[0].name, "Expense Last Year")
		self.assertEqual(check[0].ordered_id, "E-2020-1")
		check = self.acc.get_ordered_expenses(year=2021)
		self.assertEqual(len(check), 4)
		check = self.acc.get_ordered_expenses(internal=False)
		self.assertEqual(len(check), 4)
		check = self.acc.get_ordered_expenses(internal=True)
		self.assertEqual(len(check), 3)
		self.assertEqual(check[0].ordered_id, "E-0-2")
		self.assertEqual(check[1].ordered_id, "E-2021-1")
		self.assertEqual(check[2].ordered_id, "E-2021-4")
		check = self.acc.get_ordered_expenses(year=2021, internal=True)
		self.assertEqual(len(check), 2)
		self.assertEqual(check[0].ordered_id, "E-2021-1")
		self.assertEqual(check[1].ordered_id, "E-2021-4")

	def test_ordered_invoices(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.acc.save_project(v)
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.acc.save_employee(v)
		assets = []
		testdata = TestData.sorted_assets(projects=projects, employees=employees)
		for i in testdata:
			assets.append(self.acc.save_asset(i))
		check = self.acc.get_ordered_invoices()
		for i in check:
			print("{} | {}".format(i.ordered_id, i.name))
		self.assertEqual(len(check), 5)
		self.assertEqual(check[0].ordered_id, "I-0-1")
		self.assertEqual(check[1].ordered_id, "I-2020-1")
		check = self.acc.get_ordered_invoices(year=0)
		self.assertEqual(len(check), 1)
		check = self.acc.get_ordered_invoices(year=2021)
		self.assertEqual(len(check), 3)
		self.assertEqual(check[0].ordered_id, "I-2021-1")
		self.assertEqual(check[1].ordered_id, "I-2021-2")
		self.assertEqual(check[2].ordered_id, "I-2021-3")
