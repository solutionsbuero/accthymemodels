import unittest
from tests.acccpe import TestAccCPE
from tests.accassets import TestAccAssets
from tests.dbcpe import TestDbCPE
from tests.dbtime import TestDbTimeEntries
from tests.accdbsync import TestAccDbSync
from tests.gitunit import GitTests
from tests.entriesexport import TestEntriesExport
from tests.money import TestMoneyType
unittest.main()
