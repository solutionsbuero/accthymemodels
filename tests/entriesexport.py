import json
import unittest
import traceback
import shutil
import uuid
unittest.TestLoader.sortTestMethodsUsing = None
from tests.testdata import TestData
from accthymemodels.accstrategy import AccStrategy
from accthymemodels.dbstrategy import DBStrategy
from accthymemodels.mediator import DataMediator
from accthymemodels.constants import DirectoryConstants
from accthymemodels.models.timeentries import TimeEntryList, TimeEntry


class TestEntriesExport(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		pass

	@classmethod
	def tearDownClass(cls) -> None:
		pass

	def setUp(self):
		self.config = TestData.config(use_acc=True, use_git=False, use_db=True)
		self.mediator = DataMediator(self.config)
		self.acc = AccStrategy(self.mediator)
		self.db = DBStrategy(self.mediator)
		self.mediator.register_accfactory(self.acc)
		self.mediator.register_dbfactory(self.db)

	def tearDown(self) -> None:
		shutil.rmtree(self.config.acc.basepath)
		self.config.database.path.unlink()
		traceback.print_exc()

	def test_001_export(self):
		testdata = TestData.customer_creation()
		customers = {}
		for k, v in testdata.items():
			customers[k] = self.acc.save_customer(v)
		testdata = TestData.project_creation(customers)
		projects = {}
		for k, v, in testdata.items():
			projects[k] = self.acc.save_project(v)
		testdata = TestData.employee_creation()
		employees = {}
		for k, v in testdata.items():
			employees[k] = self.acc.save_employee(v)
		testdata = TestData.time_creation(projects=projects, employees=employees)
		timeentries = []
		for i in testdata:
			timeentries.append(self.db.save_timeentry(i))
		self.db.export_timeentries()
		entrylist = TimeEntryList.get_empty()
		exportpath = self.acc.config.acc.basepath / DirectoryConstants.timetracking
		for i in exportpath.iterdir():
			for j in i.iterdir():
				if j.suffix == ".json":
					with j.open("r") as f:
						jsondata = json.loads(f.read())
					for k in jsondata:
						entrylist.append(TimeEntry.parse_obj(k))
		entrylist.sort()
		for n, i in enumerate(entrylist):
			self.assertEqual(timeentries[n], entrylist.__root__[n])
